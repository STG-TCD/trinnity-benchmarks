include Makefile.config.base
include Makefile.config

.PHONY: update update-caffe-cpu update-caffe-gpu info check clean

update:
	yay -Syy trinnity-optimizer-git --answerclean A --answerdiff N
	yay -Syy trinnity-compiler-git --answerclean A --answerdiff N
	yay -Syy trinnity-git --answerclean A --answerdiff N

update-caffe-cpu:
	yay -Syy trinnity-caffe-git --answerclean A --answerdiff N

update-caffe-gpu:
	yay -Syy trinnity-caffe-cudnn-git --answerclean A --answerdiff N

info:
	@echo "SCENARIOS=$(SCENARIOS)"
	@echo "TSCENARIOS=$(TSCENARIOS)"
	@echo "METHODS=$(METHODS)"
	@echo "RUNS=$(RUNS)"
	@echo "THREADS=$(THREADS)"
	@echo "SINGLE_CORE=$(SINGLE_CORE)"
	@echo "MULTI_CORES=$(MULTI_CORES)"
	@echo "CXX=$(CXX)"
	@echo "CXXFLAGS=$(CXXFLAGS)"
	@echo "LDFLAGS=$(LDFLAGS)"
	@echo "CONFIG_FLAGS=$(CONFIG_FLAGS)"

check:
	@([ ! -e `which taskset` ] && echo "Could not find taskset" ) || echo "Found taskset at `which taskset`"
	@([ ! -e `which gnuplot` ] && echo "Could not find gnuplot" ) || echo "Found gnuplot at `which gnuplot`"
	@([ ! -e `which gawk` ] && echo "Could not find gawk" ) || echo "Found gawk at `which gawk`"
	@([ ! -e `which sed` ] && echo "Could not find sed" ) || echo "Found sed at `which sed`"
	@([ ! -e `which cat` ] && echo "Could not find cat" ) || echo "Found cat at `which cat`"
	@([ ! -e `which tail` ] && echo "Could not find tail" ) || echo "Found tail at `which tail`"
	@([ ! -e `which paste` ] && echo "Could not find paste" ) || echo "Found paste at `which paste`"
	@([ ! -e `which rm` ] && echo "Could not find rm" ) || echo "Found rm at `which rm`"
	@([ ! -e `which ln` ] && echo "Could not find ln" ) || echo "Found ln at `which ln`"
	@([ ! -e `which bc` ] && echo "Could not find bc" ) || echo "Found bc at `which bc`"
	@([ ! -e `which sudo` ] && echo "Could not find sudo" ) || echo "Found sudo at `which sudo`"
	@([ ! -e `which $(CXX)` ] && echo "Could not find $(CXX)" ) || echo "Found $(CXX) at `which $(CXX)`"
	@([ ! -e `which wget` ] && echo "Could not find wget" ) || echo "Found wget at `which wget`"
	@([ ! -e `which pbqp_solve` ] && echo "Could not find pbqp_solve" ) || echo "Found pbqp_solve at `which pbqp_solve`"

clean: build-clean results-clean stats-clean graphs-clean

build-clean:
	rm -rf $(BUILDDIR)

results-clean:
	rm -rf $(RESULTSDIR) $(RESULTSDIR)-multithreaded

stats-clean:
	rm -rf $(STATSDIR) $(STATSDIR)-multithreaded

graphs-clean:
	rm -rf $(GRAPHSDIR) $(GRAPHSDIR)-multithreaded

build-prep:
	@$(call COMMAND,mkdir -p $(BUILDDIR))
	@$(call COMMAND,ln -sf ../triNNity $(BUILDDIR)/triNNity)
	@BUILDDIR=$(BUILDDIR) SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" $(call COMMAND,./sh/build-scenarios-header.sh)

transforms-prep:
	@$(call COMMAND,mkdir -p $(BUILDDIR))
	@$(call COMMAND,ln -sf ../triNNity $(BUILDDIR)/triNNity)
	@BUILDDIR=$(BUILDDIR) SCENARIO_DEFINITIONS="$(TSCENARIO_DEFINITIONS)" SCENARIOS="$(TSCENARIOS)" $(call COMMAND,./sh/build-transforms-header.sh)

run-prep: build-prep
	@$(call COMMAND,mkdir -p $(RESULTSDIR))

cross-run-prep:
	@$(call COMMAND,mkdir -p $(RESULTSDIR))

$(BUILDDIR)/debug-scenario-%: build-prep
	@echo "Building scenario $* [debug]"
	@$(call COMMAND,$(CXX) $(DEBUG_CXXFLAGS) $(METHODS) $(CONFIG_FLAGS) -DNO_OF_RUNS=1 -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

debug: run-prep $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/debug-scenario-$(scenario))

$(BUILDDIR)/benchmark-scenario-%.pgo: build-prep
	@echo "Building scenario $* [singlethreaded, pgo]"
	@$(call COMMAND,$(CXX) -fprofile-generate=benchmark-scenario-$*.pgoinfo $(CXXFLAGS) $(METHODS) $(CONFIG_FLAGS) -DNO_OF_RUNS=3 -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/transform-scenario-%.pgo: transforms-prep
	@echo "Building scenario $* [singlethreaded transforms, pgo]"
	@$(call COMMAND,$(CXX) -fprofile-generate=transform-scenario-$*.pgoinfo $(CXXFLAGS) $(CONFIG_FLAGS) -DNO_OF_RUNS=3 -DSCENARIO=$* src/main-transforms.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/benchmark-multithreaded-scenario-%.pgo: build-prep
	@echo "Building scenario $* [multithreaded, pgo]"
	@$(call COMMAND,$(CXX) -fprofile-generate=benchmark-multithreaded-scenario-$*.pgoinfo $(CXXFLAGS) $(METHODS) -DBENCHMARK_OPENMP $(CONFIG_FLAGS) -DNO_OF_RUNS=3 -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/transform-multithreaded-scenario-%.pgo: transforms-prep
	@echo "Building scenario $* [multithreaded transforms, pgo]"
	@$(call COMMAND,$(CXX) -fprofile-generate=transform-multithreaded-scenario-$*.pgoinfo $(CXXFLAGS) -DBENCHMARK_OPENMP $(CONFIG_FLAGS) -DNO_OF_RUNS=3 -DSCENARIO=$* src/main-transforms.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

pgo-build: $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/benchmark-scenario-$(scenario).pgo)

pgo-build-multithreaded: $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/benchmark-multithreaded-scenario-$(scenario).pgo)

pgo-build-transforms: $(foreach scenario,$(TSCENARIOS),$(BUILDDIR)/transform-scenario-$(scenario).pgo)

pgo-build-multithreaded-transforms: $(foreach scenario,$(TSCENARIOS),$(BUILDDIR)/transform-multithreaded-scenario-$(scenario).pgo)

$(BUILDDIR)/benchmark-scenario-%: build-prep
	@echo "Building scenario $* [singlethreaded]"
	@$(call COMMAND,$(CXX) -fprofile-use=$(BUILDDIR)/benchmark-scenario-$*.profdata $(CXXFLAGS) $(METHODS) $(CONFIG_FLAGS) -DNO_OF_RUNS=$(RUNS) -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/transform-scenario-%: transforms-prep
	@echo "Building scenario $* [singlethreaded transforms]"
	@$(call COMMAND,$(CXX) -fprofile-use=$(BUILDDIR)/transform-scenario-$*.profdata $(CXXFLAGS) $(CONFIG_FLAGS) -DNO_OF_RUNS=$(RUNS) -DSCENARIO=$* src/main-transforms.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/benchmark-multithreaded-scenario-%: build-prep
	@echo "Building scenario $* [multithreaded]"
	@$(call COMMAND,$(CXX) -fprofile-use=$(BUILDDIR)/benchmark-multithreaded-scenario-$*.profdata $(CXXFLAGS) $(METHODS) -DBENCHMARK_OPENMP $(CONFIG_FLAGS) -DNO_OF_RUNS=$(RUNS) -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

$(BUILDDIR)/transform-multithreaded-scenario-%: transforms-prep
	@echo "Building scenario $* [multithreaded transforms]"
	@$(call COMMAND,$(CXX) -fprofile-use=$(BUILDDIR)/transform-multithreaded-scenario-$*.profdata $(CXXFLAGS) -DBENCHMARK_OPENMP $(CONFIG_FLAGS) -DNO_OF_RUNS=$(RUNS) -DSCENARIO=$* src/main-transforms.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

build: $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/benchmark-scenario-$(scenario))

build-multithreaded: $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/benchmark-multithreaded-scenario-$(scenario))

build-transforms: $(foreach scenario,$(TSCENARIOS),$(BUILDDIR)/transform-scenario-$(scenario))

build-multithreaded-transforms: $(foreach scenario,$(TSCENARIOS),$(BUILDDIR)/transform-multithreaded-scenario-$(scenario))

$(BUILDDIR)/benchmark-scenario-%.s: build-prep
	@echo "Building scenario $* [assembler]"
	@$(call COMMAND,$(CXX) $(CXXFLAGS) -fverbose-asm -S $(METHODS) $(CONFIG_FLAGS) -DNO_OF_RUNS=$(RUNS) -DSCENARIO=$* src/main.cpp -I $(BUILDDIR)/ -I src/ -o $@ $(LDFLAGS))

assembler: $(foreach scenario,$(SCENARIOS),$(BUILDDIR)/benchmark-scenario-$(scenario).s)

pgo-run-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [singlethreaded, pgo]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OPENBLAS_NUM_THREADS=1 OMP_NUM_THREADS=1 MKL_NUM_THREADS=1 sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR):$(OPENCL_LOCATION):$(CLBLAS_LOCATION):$(CLBLAST_LOCATION) taskset -c $(SINGLE_CORE) $(NICE_CMD) ./benchmark-scenario-$*.pgo; \
	sudo rm -rf mcmk-results; \
	cd - 1>/dev/null ;

pgo-run-multithreaded-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [multithreaded, pgo]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OPENBLAS_NUM_THREADS=$(THREADS) OMP_NUM_THREADS=$(THREADS) MKL_NUM_THREADS=$(THREADS) ARMCL_NUM_THREADS=$(THREADS) sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR):$(OPENCL_LOCATION):$(CLBLAS_LOCATION):$(CLBLAST_LOCATION):$(ARMCL_LOCATION) $(NICE_CMD) ./benchmark-multithreaded-scenario-$*.pgo; \
	sudo rm -rf mcmk-results; \
	cd - 1>/dev/null ;

pgo-run-transform-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [singlethreaded transforms, pgo]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR) taskset -c $(SINGLE_CORE) $(NICE_CMD) ./transform-scenario-$*.pgo; \
	sudo rm -rf transform-results; \
	cd - 1>/dev/null ;

pgo-run-multithreaded-transform-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [multithreaded transforms, pgo]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OMP_NUM_THREADS=$(THREADS) sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR) $(NICE_CMD) ./transform-multithreaded-scenario-$*.pgo; \
	sudo rm -rf transform-results; \
	cd - 1>/dev/null ;

pgo-run: run-prep $(foreach scenario,$(SCENARIOS),pgo-run-scenario-$(scenario))

pgo-run-multithreaded: run-prep $(foreach scenario,$(SCENARIOS),pgo-run-multithreaded-scenario-$(scenario))

pgo-run-transforms: $(foreach scenario,$(TSCENARIOS),pgo-run-transform-scenario-$(scenario))

pgo-run-multithreaded-transforms: $(foreach scenario,$(TSCENARIOS),pgo-run-multithreaded-transform-scenario-$(scenario))

run-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [singlethreaded]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OPENBLAS_NUM_THREADS=1 OMP_NUM_THREADS=1 MKL_NUM_THREADS=1 sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR):$(OPENCL_LOCATION):$(CLBLAS_LOCATION):$(CLBLAST_LOCATION) taskset -c $(SINGLE_CORE) $(NICE_CMD) ./benchmark-scenario-$*; \
	mkdir -p $(RESULTSDIR)/scenario/$*; \
	cp -f -r mcmk-results/* $(RESULTSDIR)/scenario/$*; \
	sudo rm -rf mcmk-results; \
	cd - 1>/dev/null ;

run-multithreaded-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [multithreaded]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OPENBLAS_NUM_THREADS=$(THREADS) OMP_NUM_THREADS=$(THREADS) MKL_NUM_THREADS=$(THREADS) ARMCL_NUM_THREADS=$(THREADS) sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR):$(OPENCL_LOCATION):$(CLBLAS_LOCATION):$(CLBLAST_LOCATION):$(ARMCL_LOCATION) $(NICE_CMD) ./benchmark-multithreaded-scenario-$*; \
	mkdir -p $(RESULTSDIR)-multithreaded/scenario/$*; \
	cp -f -r mcmk-results/* $(RESULTSDIR)-multithreaded/scenario/$*; \
	sudo rm -rf mcmk-results; \
	cd - 1>/dev/null ;

run-transform-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [singlethreaded transforms]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR) taskset -c $(SINGLE_CORE) $(NICE_CMD) ./transform-scenario-$*; \
	mkdir -p $(RESULTSDIR)/transforms/scenario/$*; \
	cp -f -r transform-results/* $(RESULTSDIR)/transforms/scenario/$*; \
	sudo rm -rf transform-results; \
	cd - 1>/dev/null ;

run-multithreaded-transform-scenario-%:
	@cd $(BUILDDIR)/; \
	echo "Running scenario $* [multithreaded transforms]"; \
	1> >(while read line; do echo -e "\e[01;32mSTDOUT: $$line\e[0m" >&1; done) 2> >(while read line; do echo -e "\e[01;31mSTDERR: $$line\e[0m" >&2; done) \
	OMP_NUM_THREADS=$(THREADS) sudo -E LD_LIBRARY_PATH=$(MKLDNN_MKL_LIBDIR) $(NICE_CMD) ./transform-multithreaded-scenario-$*; \
	mkdir -p $(RESULTSDIR)-multithreaded/transforms/scenario/$*; \
	cp -f -r transform-results/* $(RESULTSDIR)-multithreaded/transforms/scenario/$*; \
	sudo rm -rf transform-results; \
	cd - 1>/dev/null ;

run: run-prep $(foreach scenario,$(SCENARIOS),run-scenario-$(scenario))

run-multithreaded: run-prep $(foreach scenario,$(SCENARIOS),run-multithreaded-scenario-$(scenario))

run-transforms: $(foreach scenario,$(TSCENARIOS),run-transform-scenario-$(scenario))

run-multithreaded-transforms: $(foreach scenario,$(TSCENARIOS),run-multithreaded-transform-scenario-$(scenario))

cross-run: cross-run-prep $(foreach scenario,$(CROSS_SCENARIOS),run-scenario-$(scenario))

cross-run-multithreaded: cross-run-prep $(foreach scenario,$(CROSS_SCENARIOS),run-multithreaded-scenario-$(scenario))

cross-run-transforms: $(foreach scenario,$(CROSS_TSCENARIOS),run-transform-scenario-$(scenario))

cross-run-multithreaded-transforms: $(foreach scenario,$(CROSS_TSCENARIOS),run-multithreaded-transform-scenario-$(scenario))

plot-clean:
	rm -rf $(GRAPHSDIR)

.PHONY: stats stats-energy stats-multithreaded stats-multithreaded-energy stats-transforms stats-multithreaded-transforms csv csv-multithreaded csv-transforms csv-multithreaded-transforms plot-prep plot-all plot

stats:
	@DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)/" STATS="$(STATSDIR)/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" SCENARIO_NAMES="$(SCENARIO_NAMES)" $(call COMMAND,./sh/compute-stats.sh)

stats-energy:
	@RUNS=$(RUNS) DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)/" STATS="$(STATSDIR)/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" SCENARIO_NAMES="$(SCENARIO_NAMES)" $(call COMMAND,./sh/extract-energy.sh)

stats-multithreaded:
	@DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)-multithreaded/" STATS="$(STATSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" SCENARIO_NAMES="$(SCENARIO_NAMES)" $(call COMMAND,./sh/compute-stats.sh)

stats-multithreaded-energy:
	@RUNS=$(RUNS) DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)-multithreaded/" STATS="$(STATSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" SCENARIO_NAMES="$(SCENARIO_NAMES)" $(call COMMAND,./sh/extract-energy.sh)

stats-transforms:
	@DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)/" STATS="$(STATSDIR)/" GRAPH_METRICS="$(METRICS)" SCENARIO_DEFINITIONS="$(TSCENARIO_DEFINITIONS)" SCENARIOS="$(TSCENARIOS)" $(call COMMAND,./sh/compute-stats-transforms.sh)

stats-multithreaded-transforms:
	@DISCARD=$(DISCARD) RESULTS="$(RESULTSDIR)-multithreaded/" STATS="$(STATSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" SCENARIO_DEFINITIONS="$(TSCENARIO_DEFINITIONS)" SCENARIOS="$(TSCENARIOS)" $(call COMMAND,./sh/compute-stats-transforms.sh)

csv: stats
	@STATS="$(STATSDIR)/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" $(call COMMAND,./sh/build-csv.sh)

csv-multithreaded: stats-multithreaded
	@STATS="$(STATSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(ENABLE_METHODS)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(SCENARIOS)" $(call COMMAND,./sh/build-csv.sh)

csv-transforms: stats-transforms
	@STATS="$(STATSDIR)/" GRAPH_METRICS="$(METRICS)" SCENARIO_DEFINITIONS="$(TSCENARIO_DEFINITIONS)" SCENARIOS="$(TSCENARIOS)" $(call COMMAND,./sh/build-csv-transforms.sh)

csv-multithreaded-transforms: stats-multithreaded-transforms
	@STATS="$(STATSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" SCENARIO_DEFINITIONS="$(TSCENARIO_DEFINITIONS)" SCENARIOS="$(TSCENARIOS)" $(call COMMAND,./sh/build-csv-transforms.sh)

plot-prep:
	@mkdir -p $(GRAPHSDIR)
	@mkdir -p $(GRAPHSDIR)-multithreaded

plot-all: plot-prep stats stats-multithreaded
	@STATS="$(STATSDIR)/" GRAPHS="$(GRAPHSDIR)/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(PLOT_METHODS)" TITLE="$(PLOT_TITLE)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(PLOT_SCENARIOS)" $(call COMMAND,./sh/bar-graph.sh)
	@STATS="$(STATSDIR)-multithreaded/" GRAPHS="$(GRAPHSDIR)-multithreaded/" GRAPH_METRICS="$(METRICS)" GRAPH_METHODS="$(PLOT_METHODS)" TITLE="$(PLOT_TITLE) (multithreaded)" SCENARIO_DEFINITIONS="$(SCENARIO_DEFINITIONS)" SCENARIOS="$(PLOT_SCENARIOS)" $(call COMMAND,./sh/bar-graph.sh)

plot: plot-prep plot-all

#########################################
# These are benchmarking configurations #
#########################################

pgo-end-to-end-singlethreaded-%:
	@rm -f Makefile.config; echo > Makefile.config; sync
	@ln -sf Makefile.config.$*-transforms Makefile.config
	@$(MAKE) -s -C . pgo-build-transforms
	@$(MAKE) -j 1 -C . pgo-run-transforms
	@$(MAKE) -s -C . build-transforms
	@$(MAKE) -j 1 -C . run-transforms
	@$(MAKE) -j 1 -C . -B csv-transforms
	@ln -sf Makefile.config.$* Makefile.config
	@$(MAKE) -s -C . pgo-build
	@$(MAKE) -j 1 -C . pgo-run
	@$(MAKE) -s -C . build
	@$(MAKE) -j 1 -C . run
	@$(MAKE) -j 1 -C . -B csv

pgo-end-to-end-multithreaded-%:
	@rm -f Makefile.config; echo > Makefile.config; sync
	@ln -sf Makefile.config.$*-transforms Makefile.config
	@$(MAKE) -s -C . pgo-build-multithreaded-transforms
	@$(MAKE) -j 1 -C . pgo-run-multithreaded-transforms
	@$(MAKE) -s -C . build-multithreaded-transforms
	@$(MAKE) -j 1 -C . run-multithreaded-transforms
	@$(MAKE) -j 1 -C . -B csv-multithreaded-transforms
	@ln -sf Makefile.config.$* Makefile.config
	@$(MAKE) -s -C . pgo-build-multithreaded
	@$(MAKE) -j 1 -C . pgo-run-multithreaded
	@$(MAKE) -s -C . build-multithreaded
	@$(MAKE) -j 1 -C . run-multithreaded
	@$(MAKE) -j 1 -C . -B csv-multithreaded

pgo-end-to-end-%: pgo-end-to-end-singlethreaded-% pgo-end-to-end-multithreaded-%

end-to-end-singlethreaded-%:
	@rm -f Makefile.config; echo > Makefile.config; sync
	@ln -sf Makefile.config.$*-transforms Makefile.config
	@$(MAKE) -s -C . build-transforms
	@$(MAKE) -j 1 -C . run-transforms
	@$(MAKE) -j 1 -C . -B csv-transforms
	@ln -sf Makefile.config.$* Makefile.config
	@$(MAKE) -s -C . build
	@$(MAKE) -j 1 -C . run
	@$(MAKE) -j 1 -C . -B csv

end-to-end-multithreaded-%:
	@rm -f Makefile.config; echo > Makefile.config; sync
	@ln -sf Makefile.config.$*-transforms Makefile.config
	@$(MAKE) -s -C . build-multithreaded-transforms
	@$(MAKE) -j 1 -C . run-multithreaded-transforms
	@$(MAKE) -j 1 -C . -B csv-multithreaded-transforms
	@ln -sf Makefile.config.$* Makefile.config
	@$(MAKE) -s -C . build-multithreaded
	@$(MAKE) -j 1 -C . run-multithreaded
	@$(MAKE) -j 1 -C . -B csv-multithreaded

end-to-end-%: end-to-end-singlethreaded-% end-to-end-multithreaded-%

cross-end-to-end-singlethreaded-%:
	rm -f Makefile.config; echo > Makefile.config; sync
	ln -sf Makefile.config.$*-transforms Makefile.config
	ssh -t $(CROSS_BENCHMARK_HOST) "sudo rm -rf $(CROSS_BENCHMARK_PATH) && mkdir -p $(CROSS_BENCHMARK_PATH)"
	$(MAKE) -B -C $(shell dirname $(SCENARIO_DEFINITIONS)) scenarios transforms
	-rsync --quiet -avP --delete --ignore-errors $(shell dirname $(SCENARIO_DEFINITIONS)) $(CROSS_BENCHMARK_HOST):$(shell dirname $(CROSS_BENCHMARK_PATH))
	-rsync --quiet -avP --delete --ignore-errors Makefile* $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	-rsync --quiet -avP --delete --ignore-errors sh/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/sh/
	$(MAKE) -s -C . build-transforms
	-rsync --quiet -avP --delete --ignore-errors $(BUILDDIR)/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(BUILDDIR))))/
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH) && make -j 1 -C . cross-run-transforms"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(RESULTSDIR))))/ $(RESULTSDIR)/
	$(MAKE) -j 1 -C . -B csv-transforms
	ln -sf Makefile.config.$* Makefile.config
	$(MAKE) -s -C . build
	-rsync --quiet -avP --delete --ignore-errors $(BUILDDIR)/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(BUILDDIR))))/
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH) && make -j 1 -C . cross-run"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(RESULTSDIR))))/ $(RESULTSDIR)/
	$(MAKE) -j 1 -C . -B csv

cross-end-to-end-multithreaded-%:
	rm -f Makefile.config; echo > Makefile.config; sync
	ln -sf Makefile.config.$*-transforms Makefile.config
	ssh -t $(CROSS_BENCHMARK_HOST) "sudo rm -rf $(CROSS_BENCHMARK_PATH) && mkdir -p $(CROSS_BENCHMARK_PATH)"
	$(MAKE) -B -C $(shell dirname $(SCENARIO_DEFINITIONS)) scenarios transforms
	-rsync --quiet -avP --delete --ignore-errors $(shell dirname $(SCENARIO_DEFINITIONS)) $(CROSS_BENCHMARK_HOST):$(shell dirname $(CROSS_BENCHMARK_PATH))
	-rsync --quiet -avP --delete --ignore-errors Makefile* $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	-rsync --quiet -avP --delete --ignore-errors sh/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/sh/
	$(MAKE) -s -C . build-multithreaded-transforms
	-rsync --quiet -avP --delete --ignore-errors $(BUILDDIR)/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(BUILDDIR))))/
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH) && make -j 1 -C . cross-run-multithreaded-transforms"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(RESULTSDIR)-multithreaded)))/ $(RESULTSDIR)-multithreaded/
	$(MAKE) -j 1 -C . -B csv-multithreaded-transforms
	ln -sf Makefile.config.$* Makefile.config
	$(MAKE) -s -C . build-multithreaded
	-rsync --quiet -avP --delete --ignore-errors $(BUILDDIR)/ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(BUILDDIR))))/
	ssh -t $(CROSS_BENCHMARK_HOST) "cd $(CROSS_BENCHMARK_PATH) && make -j 1 -C . cross-run-multithreaded"
	-rsync --quiet -avP $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/$(shell echo $$(basename $$(realpath $(RESULTSDIR)-multithreaded)))/ $(RESULTSDIR)-multithreaded/
	$(MAKE) -j 1 -C . -B csv-multithreaded

cross-end-to-end-%: cross-end-to-end-singlethreaded-% cross-end-to-end-multithreaded-%
