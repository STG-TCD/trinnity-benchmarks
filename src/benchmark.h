/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <algorithm>
#include <iostream>
#include <limits>
#include <type_traits>

#include "config.h"
#include "benchmark-utils.h"

template <typename G, typename T, typename KD, typename C, unsigned k_w, unsigned k_h, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned stride, unsigned input_bits=32, unsigned output_bits=32>
struct MCMKBenchmarkConfig : public benchmark::BenchmarkConfig {

  C* ground_truth_backing_array;

  G* gt_test_image;
  T* test_image;
  KD* kernel;

  const C* ground_truth;

  MCMKBenchmarkConfig<G, T, KD, C, k_w, k_h, kernels, target_channels, img_w, img_h, stride, input_bits, output_bits>(uint64_t r, std::string s, double sparsity, unsigned kvr, unsigned ivr)
  : benchmark::BenchmarkConfig(r, s) {

    static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
    static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

    // Allocate the output buffer for the ground truth
    ground_truth_backing_array = new C[kernels*out_h*out_w]();

    // Set up the handle
    ground_truth = ground_truth_backing_array;

    // Randomize the kernel
    kernel = triNNity::dense::cpu::memory::buildSparseRandomKernel<KD, k_w, k_h, kernels, target_channels, triNNity::layout::OIHW>(sparsity, kvr);

    // Randomize the test image
    test_image = triNNity::dense::cpu::memory::buildRandomKernel<T, 1, 1, target_channels, img_h*img_w, triNNity::layout::OIHW>(ivr);


    #ifdef ENFORCE_INPUT_BITS
    // If the image type is integral, enforce input_bits
    if (std::is_integral<T>::value) {
      auto imageView = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(test_image);
      for (unsigned c = 0; c < target_channels; c++) {
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            imageView[c][h][w] &= ((1 << input_bits) - 1);
          }
        }
      }
    }

    // If the kernel type is integral, enforce input_bits
    if (std::is_integral<KD>::value) {
      auto kernelView = triNNity::internal::memory::View4D<KD, k_h, k_w, kernels, target_channels>(kernel);
      for (unsigned ky = 0; ky < k_h; ky++) {
        for (unsigned kx = 0; kx < k_w; kx++) {
          for (unsigned m = 0; m < kernels; m++) {
            for (unsigned c = 0; c < target_channels; c++) {
              kernelView[ky][kx][m][c] &= ((1 << input_bits) - 1);
            }
          }
        }
      }
    }
    #endif // ENFORCE_INPUT_BITS

    // Copy the randomized test image into the ground truth precision
    gt_test_image = new G[target_channels * img_h * img_w]();
    std::transform(test_image, test_image+(target_channels*img_h*img_w), gt_test_image,
                   [](T x) -> G { return (G)x; });

    // Compute the ground truth
    triNNity::dense::convolution_forward_direct<G, C, C, KD, img_w, img_h, target_channels, kernels, k_w, k_h,
                                                triNNity::CONV_MULTI_SUM_SINGLE, CONV_BOUND_METHOD,
                                                triNNity::layout::CHW,
                                                triNNity::layout::OIHW,
                                                triNNity::layout::CHW,
                                                stride, stride>
                                                (gt_test_image, kernel, ground_truth_backing_array);


    #ifdef ENFORCE_OUTPUT_BITS
    // If the ground truth image type is integral, enforce output_bits
    if (std::is_integral<G>::value) {
      auto outputView = triNNity::internal::memory::View3D<C, kernels, out_h, out_w>(ground_truth_backing_array);
      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            outputView[m][h][w] &= ((1 << output_bits) - 1);
          }
        }
      }
    }
    #endif // ENFORCE_OUTPUT_BITS
  }

  ~MCMKBenchmarkConfig() {
    delete [] gt_test_image; gt_test_image = nullptr;
    delete [] test_image; test_image = nullptr;
    delete [] kernel; kernel = nullptr;
    delete [] ground_truth_backing_array; ground_truth_backing_array = nullptr;
  }
};

template <typename T, typename KD, typename C, typename A, unsigned pixel_bits, unsigned k_w, unsigned k_h, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned stride>
class MCMKBenchmark : public benchmark::Benchmark {

protected:
  // Hang on to reorganized versions of these
  // to avoid doing pointless extra work.

  T* image_reorganized;

  A* output_data;
  A* output_data_reorganized;

  KD* kernel_reorganized_kkmc;
  KD* kernel_reorganized_ckkm;
  KD* kernel_reorganized_mckk;
  KD* kernel_reorganized_kkcm;
  KD* kernel_reorganized_mkkc;

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

public:

  virtual void reorganize_image(benchmark::BenchmarkConfig*) = 0;
  virtual void reorganize_output() = 0;
  virtual void reorganize_kernel(benchmark::BenchmarkConfig*) = 0;

  virtual void setup(benchmark::BenchmarkConfig *cfg) {

    // Might allocate image_reorganized
    this->reorganize_image(cfg);

    // Might allocate any of the reorganized kernels
    this->reorganize_kernel(cfg);
  }

  virtual void teardown(benchmark::BenchmarkConfig *cfg) {};

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) = 0;

  virtual std::string name() = 0;

  MCMKBenchmark() {
    // Allocate space for our output
    this->output_data = new A[kernels * out_h * out_w];

    // Initialize all these to nullptr, derived benchmarks may allocate them
    image_reorganized = nullptr;
    output_data_reorganized = nullptr;
    kernel_reorganized_kkmc = nullptr;
    kernel_reorganized_ckkm = nullptr;
    kernel_reorganized_mckk = nullptr;
    kernel_reorganized_kkcm = nullptr;
    kernel_reorganized_mkkc = nullptr;

  }

  ~MCMKBenchmark() {
    delete [] this->output_data; this->output_data = nullptr;

    if (image_reorganized != nullptr) {
      delete [] image_reorganized; image_reorganized = nullptr;
    }

    if (output_data_reorganized != nullptr) {
      delete [] output_data_reorganized; output_data_reorganized = nullptr;
    }

    if (kernel_reorganized_kkmc != nullptr) {
      delete [] kernel_reorganized_kkmc; kernel_reorganized_kkmc = nullptr;
    }

    if (kernel_reorganized_ckkm != nullptr) {
      delete [] kernel_reorganized_ckkm; kernel_reorganized_ckkm = nullptr;
    }

    if (kernel_reorganized_mckk != nullptr) {
      delete [] kernel_reorganized_mckk; kernel_reorganized_mckk = nullptr;
    }

    if (kernel_reorganized_kkcm != nullptr) {
      delete [] kernel_reorganized_kkcm; kernel_reorganized_kkcm = nullptr;
    }

    if (kernel_reorganized_mkkc != nullptr) {
      delete [] kernel_reorganized_mkkc; kernel_reorganized_mkkc = nullptr;
    }
  }
};

template <typename G, typename T, typename KD, typename C, typename A, unsigned input_bits, unsigned k_w, unsigned k_h, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned stride, unsigned output_bits=32>
class ComparisonMCMKBenchmark : public MCMKBenchmark<T, KD, C, A, input_bits, k_w, k_h, kernels, target_channels, img_w, img_h, stride> {

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

public:
  virtual void reorganize_image(){}
  virtual void reorganize_output() = 0;
  virtual void reorganize_kernel(benchmark::BenchmarkConfig*){}

  virtual void teardown(benchmark::BenchmarkConfig *cfg) {
    this->reorganize_output();

    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k_w, k_h, kernels, target_channels, img_w, img_h, stride, input_bits, output_bits>*>(cfg);

    auto test_data = triNNity::internal::memory::View3D<A, kernels, out_h, out_w>(this->output_data);
    auto gt_data = triNNity::internal::memory::View3D<const C, kernels, out_h, out_w>(config->ground_truth);

    #ifdef ENFORCE_OUTPUT_BITS
    // If the output image type is integral, enforce output_bits
    if (std::is_integral<A>::value) {
      for (unsigned m = 0; m < kernels; m++) {
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            test_data[m][h][w] &= ((1 << output_bits) - 1);
          }
        }
      }
    }
    #endif // ENFORCE_OUTPUT_BITS

    #if defined(BENCHMARK_PRINT_ARRAYS)
    std::cout << std::endl << "In " << this->name() << ": " << std::endl;

    std::cout << std::endl << "Kernel is: " << std::endl;
    benchmark::print2D(k, k*kernels*target_channels, config->kernel);

    std::cout << std::endl << "Input is: " << std::endl;
    benchmark::print2D(target_channels*img_w, img_h, config->test_image);

    std::cout << std::endl << "Result is: " << std::endl;
    benchmark::print2D(kernels*out_w, out_h, this->output_data);

    std::cout << std::endl << "Correct result is: " << std::endl;
    benchmark::print2D(kernels*out_w, out_h, config->ground_truth);
    #endif // defined(BENCHMARK_PRINT_ARRAYS)

    #if defined(BENCHMARK_ERROR_FULL)
    {
      double total_err_full = 0.0;
      uint64_t total_distinct_full = 0;
      constexpr uint64_t total_pixels_full = kernels * out_h * out_w;

      double *diffs = new double[total_pixels_full]();
      uint64_t *distinct = new uint64_t[total_pixels_full]();

      // Compute pointwise absolute difference between the two buffers
      std::transform(gt_data.begin(), gt_data.end(), test_data.begin(), diffs,
                     [](C a, A b) -> double { return std::abs((double)a - (double)b); });

      // Sum the absolute differences
      total_err_full = std::accumulate(diffs, diffs+total_pixels_full, 0);

      // Compute the number of distinct pixels
      std::transform(gt_data.begin(), gt_data.end(), test_data.begin(), distinct,
                     [](C a, A b) -> uint64_t { return (a != b); });

      // Sum the ones from distinctness buffer
      total_distinct_full = std::accumulate(distinct, distinct+total_pixels_full, 0);

      delete [] diffs; diffs = nullptr;
      delete [] distinct; distinct = nullptr;

      double err_pixel = ((double)total_err_full/(double)total_pixels_full);

      if (std::abs(err_pixel) > 1.0) {
        std::cerr << std::endl<< this->name() << " d_total=" << total_distinct_full
                                              << " d_total(%)=" << 100.0 * ((double)total_distinct_full/(double)total_pixels_full)
                                              << " e_total=" << total_err_full
                                              << " e_kernel=" << ((double)total_err_full/(double)kernels)
                                              << " e_pixel=" << ((double)total_err_full/(double)total_pixels_full)
                                              << std::endl;

        #if !defined(BENCHMARK_PRESERVE_ALL_TIMINGS)
        config->wipeResultsFile(this->name().c_str());
        #endif
      } else {
        std::cout << std::endl<< this->name() << " d_total=" << total_distinct_full
                                              << " d_total(%)=" << 100.0 * ((double)total_distinct_full/(double)total_pixels_full)
                                              << " e_total=" << total_err_full
                                              << " e_kernel=" << ((double)total_err_full/(double)kernels)
                                              << " e_pixel=" << ((double)total_err_full/(double)total_pixels_full)
                                              << std::endl;
      }
    }
    #endif

    #if defined(BENCHMARK_ERROR_INTERIOR)
    {
      double total_err_full = 0.0;
      uint64_t total_distinct_full = 0;
      constexpr uint64_t total_pixels_full = kernels * out_h * out_w;

      double *diffs = new double[total_pixels_full]();
      uint64_t *distinct = new uint64_t[total_pixels_full]();

      auto diff_data = triNNity::internal::memory::View3D<double, kernels, out_h, out_w>(diffs);
      auto distinct_data = triNNity::internal::memory::View3D<uint64_t, kernels, out_h, out_w>(distinct);

      // Compute pointwise absolute difference between the two buffers
      std::transform(gt_data.begin(), gt_data.end(), test_data.begin(), diffs,
                     [](C a, A b) -> double { return std::abs(a - b); });

      // Zero the error for boundary pixels
      for (unsigned m = 0; m < kernels; m++) {

        // Left side
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < k_w/2; w++) {
            diff_data[m][h][w] = 0;
          }
        }

        // Right side
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = img_w-(k_w/2); w < img_w; w++) {
            diff_data[m][h][w] = 0;
          }
        }

        // Top
        for (unsigned h = 0; h < k_h/2; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            diff_data[m][h][w] = 0;
          }
        }

        // Bottom
        for (unsigned h = img_h-(k_h/2); h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            diff_data[m][h][w] = 0;
          }
        }
      }

      // Sum the absolute differences
      total_err_full = std::accumulate(diffs, diffs+total_pixels_full, 0);

      // Compute the number of distinct pixels
      std::transform(gt_data.begin(), gt_data.end(), test_data.begin(), distinct,
                     [](C a, A b) -> uint64_t { return (a != b); });

      // Zero the distinct bit for boundary pixels
      for (unsigned m = 0; m < kernels; m++) {

        // Left side
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = 0; w < k_w/2; w++) {
            distinct_data[m][h][w] = 0;
          }
        }

        // Right side
        for (unsigned h = 0; h < img_h; h++) {
          for (unsigned w = img_w-(k_w/2); w < img_w; w++) {
            distinct_data[m][h][w] = 0;
          }
        }

        // Top
        for (unsigned h = 0; h < k_h/2; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            distinct_data[m][h][w] = 0;
          }
        }

        // Bottom
        for (unsigned h = img_h-(k_h/2); h < img_h; h++) {
          for (unsigned w = 0; w < img_w; w++) {
            distinct_data[m][h][w] = 0;
          }
        }
      }

      // Sum the ones from distinctness buffer
      total_distinct_full = std::accumulate(distinct, distinct+total_pixels_full, 0);

      delete [] diffs; diffs = nullptr;
      delete [] distinct; distinct = nullptr;

      double err_pixel = ((double)total_err_full/(double)total_pixels_full);

      if (std::abs(err_pixel) > 1.0) {
        std::cerr << std::endl<< this->name() << " d_total=" << total_distinct_full
                                              << " d_total(%)=" << 100.0 * ((double)total_distinct_full/(double)total_pixels_full)
                                              << " e_total=" << total_err_full
                                              << " e_kernel=" << ((double)total_err_full/(double)kernels)
                                              << " e_pixel=" << ((double)total_err_full/(double)total_pixels_full)
                                              << std::endl;

        #if !defined(BENCHMARK_PRESERVE_ALL_TIMINGS)
        config->wipeResultsFile(this->name().c_str());
        #endif
      } else {
        std::cout << std::endl<< this->name() << " d_total=" << total_distinct_full
                                              << " d_total(%)=" << 100.0 * ((double)total_distinct_full/(double)total_pixels_full)
                                              << " e_total=" << total_err_full
                                              << " e_kernel=" << ((double)total_err_full/(double)kernels)
                                              << " e_pixel=" << ((double)total_err_full/(double)total_pixels_full)
                                              << std::endl;
      }
    }
    #endif

    MCMKBenchmark<T, KD, C, A, input_bits, k_w, k_h, kernels, target_channels, img_w, img_h, stride>::teardown(cfg);
  }

  ComparisonMCMKBenchmark() : MCMKBenchmark<T, KD, C, A, input_bits, k_w, k_h, kernels, target_channels, img_w, img_h, stride>() {}
};

#endif // BENCHMARK_H
