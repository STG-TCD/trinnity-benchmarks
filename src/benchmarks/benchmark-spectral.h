/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#include "config.h"
#include "benchmark.h"

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned ofms, unsigned ifms, unsigned img_w, unsigned img_h,
          unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned winograd_impl, unsigned tv_lanes=1, typename TV=T,
          unsigned kdv_lanes=1, typename KDV=KD, bool measure_kernel_transform=true>
class WinogradMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride> {
  std::string implName;
public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);

    this->image_reorganized = new T[img_h * img_w * ifms];
    auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, ifms>(this->image_reorganized);
    auto input_view = triNNity::internal::memory::View3D<T, ifms, img_h, img_w>(config->test_image);

    for (unsigned y = 0; y < img_h; y++) {
      for (unsigned x = 0; x < img_w; x++) {
        for (unsigned c = 0; c < ifms; c++) {
          reorganized_view[y][x][c] = input_view[c][y][x];
        }
      }
    }
  }

  virtual void reorganize_output(){
    benchmark::transpose2D<T, triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>(), ofms>(this->output_data_reorganized, this->output_data);
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig* cfg){ //hwoi / kkmc
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);
    if (!measure_kernel_transform) {
      KD* kerTemp = new KD[ifms*k*k*ofms];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, ofms, ifms>(config->kernel, kerTemp);
      switch (winograd_impl) {
        case triNNity::CONV_MULTI_WINOGRAD_2: {
          constexpr unsigned tile_size = (k+2)-1;
          switch (k) {
            case 3: {
              this->kernel_reorganized_kkmc = new KD[ifms*k*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_2_3_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            case 5: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_2_5_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            default: {
              TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
            } break;
          }
        } break;
        case triNNity::CONV_MULTI_WINOGRAD_3: {
          constexpr unsigned tile_size = (k+3)-1;
          switch (k) {
            case 3: {
              this->kernel_reorganized_kkmc = new KD[ifms*k*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_3_3_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            case 5: {
              this->kernel_reorganized_kkmc = new KD[ifms*k*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_3_5_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            default: {
              TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
            } break;
          }
        } break;

        case triNNity::CONV_MULTI_WINOGRAD_2x2: {
          constexpr unsigned tile_size = (k+2)-1;
          switch (k) {
            case 3: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_2x2_3x3_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            case 5: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_2x2_5x5_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            default: {
              TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
            } break;
          }
        } break;

        case triNNity::CONV_MULTI_WINOGRAD_3x3: {
          constexpr unsigned tile_size = (k+3)-1;
          switch (k) {
            case 3: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_3x3_3x3_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            case 5: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_3x3_5x5_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            default: {
              TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
            } break;
          }
        } break;

        case triNNity::CONV_MULTI_WINOGRAD_4x4: {
          constexpr unsigned tile_size = (k+4)-1;
          switch (k) {
            case 3: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_4x4_3x3_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            case 5: {
              this->kernel_reorganized_kkmc = new KD[ifms*tile_size*tile_size*ofms];
              triNNity::spectral::cpu::transform::winograd_4x4_5x5_kernel_to_winograd<KD, ifms, ofms, kdv_lanes, KDV>((KDV*)kerTemp, (KDV*)this->kernel_reorganized_kkmc);
            } break;
            default: {
              TRINNITY_ERROR("Not implemented. Winograd based methods only currently support k=3 and k=5.");
            } break;
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
      delete [] kerTemp;
    } else {
      this->kernel_reorganized_kkmc = new KD[ifms*k*k*ofms];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, ofms, ifms>(config->kernel, this->kernel_reorganized_kkmc);
    }
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    KDV* kernel = (KDV*)this->kernel_reorganized_kkmc;
    TV* image = (TV*)this->image_reorganized;
    TV* output = (TV*)this->output_data_reorganized;
    triNNity::spectral::convolution_forward_winograd<T, KD, img_w, img_h, ifms, ofms, k,
                                              (triNNity::conv_winograd_impl_t) winograd_impl, CONV_BOUND_METHOD,
                                              (triNNity::gemm_variant_t) gemm_var,
                                              (triNNity::layout::feature_map_layout_t) img_layout,
                                              (triNNity::layout::parameter_layout_t)   krn_layout,
                                              (triNNity::layout::feature_map_layout_t) out_layout,
                                              stride, stride,
                                              tv_lanes, TV, kdv_lanes, KDV, measure_kernel_transform>
                                              (image, kernel, output);
  }

  virtual std::string name() { return implName; }

  WinogradMCMKBenchmark(std::string n) : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride>() {
    this->output_data_reorganized = new T[ofms*triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>()];
    implName = n;
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned ofms, unsigned ifms, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class FFTStitchedRealBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride> {
  std::complex<T>  *image_chf;
  std::complex<KD> *kernel_oihf;
  std::complex<T>  *output_mhf;

  static constexpr unsigned stride_w = stride;
  static constexpr unsigned stride_h = stride;

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<out_w, k>();

  public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {}
  virtual void reorganize_output() {}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig* cfg) {}

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);

    triNNity::spectral::cpu::transform::image_chw_to_chf_real<T, img_h, img_w, ifms, k, fourier_domain_length, stride_w>(config->test_image, image_chf);

    triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_real<KD, k, ofms, ifms, fourier_domain_length, stride_w>(config->kernel, kernel_oihf);

    triNNity::spectral::convolution_forward_fourier<T, KD, img_w, img_h, ifms, ofms, k, fourier_domain_length,
                                                  triNNity::CONV_MULTI_FFT_STITCHED_REAL, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var,
                                                  (triNNity::layout::spectral_feature_map_layout_t) img_layout,
                                                  (triNNity::layout::spectral_parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::spectral_feature_map_layout_t) out_layout,
                                                  stride_w, stride_h>
                                                  (image_chf, kernel_oihf, output_mhf);

    triNNity::spectral::cpu::transform::out_mhf_to_mhw_real<T, img_h, img_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_mhf, this->output_data);
  }

  virtual std::string name() { return "fft-stitched-real"; }

  FFTStitchedRealBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride>() {
    image_chf   = new std::complex<T>[ifms*stride_w*img_h*(fourier_domain_length/2 + 1)];
    kernel_oihf = new std::complex<KD>[ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1)];
    output_mhf  = new std::complex<T>[ofms*out_h*(fourier_domain_length/2 + 1)]();
  }
  ~FFTStitchedRealBenchmark() {
    delete [] image_chf;
    delete [] kernel_oihf;
    delete [] output_mhf;
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned ofms, unsigned ifms, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class FFTStitchedComplexBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride> {
  std::complex<T>  *image_chf;
  std::complex<KD> *kernel_oihf;
  std::complex<T>  *output_mhf;

  static constexpr unsigned stride_w = stride;
  static constexpr unsigned stride_h = stride;

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<out_w, k>();

  public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {}
  virtual void reorganize_output() {}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig* cfg) {}

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);

    triNNity::spectral::cpu::transform::image_chw_to_chf_complex<T, img_h, img_w, ifms, k, fourier_domain_length, stride_w>(config->test_image, image_chf);

    triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_complex<KD, k, ofms, ifms, fourier_domain_length, stride_w>(config->kernel, kernel_oihf);

    triNNity::spectral::convolution_forward_fourier<T, KD, img_w, img_h, ifms, ofms, k, fourier_domain_length,
                                                  triNNity::CONV_MULTI_FFT_STITCHED_COMPLEX, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var,
                                                  (triNNity::layout::spectral_feature_map_layout_t) img_layout,
                                                  (triNNity::layout::spectral_parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::spectral_feature_map_layout_t) out_layout,
                                                  stride_w, stride_h>
                                                  (image_chf, kernel_oihf, output_mhf);

    triNNity::spectral::cpu::transform::out_mhf_to_mhw_complex<T, img_h, img_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_mhf, this->output_data);
  }

  virtual std::string name() { return "fft-stitched-complex"; }

  FFTStitchedComplexBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride>() {
    image_chf     = new std::complex<T>[ifms*stride_w*img_h*fourier_domain_length];
    kernel_oihf   = new std::complex<KD>[ofms*ifms*stride_w*k*fourier_domain_length];
    output_mhf    = new std::complex<T>[ofms*out_h*fourier_domain_length]();
  }
  ~FFTStitchedComplexBenchmark() {
    delete [] image_chf;
    delete [] kernel_oihf;
    delete [] output_mhf;
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned ofms, unsigned ifms, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class FFTFullGemmRealBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride> {
  std::complex<T>  *image_chf;
  std::complex<T>  *img_fhcp;
  std::complex<KD> *kernel_oihf;
  std::complex<KD> *kernel_fhoip;
  std::complex<T>  *output_fhoy;
  std::complex<T>  *output_ohf;

  static constexpr unsigned stride_w = stride;
  static constexpr unsigned stride_h = stride;

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

  static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<img_w, k>();

  public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {}
  virtual void reorganize_output() {}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig* cfg) {}

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);

    triNNity::spectral::cpu::transform::image_chw_to_chf_real<T, img_h, img_w, ifms, k, fourier_domain_length, stride_w>(config->test_image, image_chf);
    triNNity::spectral::cpu::transform::image_chfp_to_fhcp<T, ifms, img_h, fourier_domain_length, stride_w, triNNity::FFT_R2C>(image_chf, img_fhcp);

    triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_real<KD, k, ofms, ifms, fourier_domain_length, stride_w>(config->kernel, kernel_oihf);
    triNNity::spectral::cpu::transform::kernel_oihfp_to_fohip<KD, ofms, ifms, k, fourier_domain_length, stride_w, triNNity::FFT_R2C>(kernel_oihf, kernel_fhoip);


    triNNity::spectral::convolution_forward_fourier<T, KD, img_w, img_h, ifms, ofms, k, fourier_domain_length,
                                                  triNNity::CONV_MULTI_FFT_FULL_GEMM_REAL, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var,
                                                  (triNNity::layout::spectral_feature_map_layout_t) img_layout,
                                                  (triNNity::layout::spectral_parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::spectral_feature_map_layout_t) out_layout,
                                                  stride, stride>
                                                  (img_fhcp, kernel_fhoip, output_fhoy);

    triNNity::spectral::cpu::transform::out_fhoyp_to_ohfp<T, fourier_domain_length, img_h, k, ofms, stride_w, stride_h, triNNity::FFT_R2C>(output_fhoy, output_ohf);
    triNNity::spectral::cpu::transform::out_mhf_to_mhw_real<T, img_h, img_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_ohf, this->output_data);
  }

  virtual std::string name() { return "fft-full-gemm-real"; }

  FFTFullGemmRealBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride>() {
    image_chf    = new std::complex<T>[ifms*stride_w*img_h*(fourier_domain_length/2 + 1)];
    img_fhcp     = new std::complex<T>[(fourier_domain_length/2 + 1)*img_h*ifms*stride_w];
    kernel_oihf  = new std::complex<KD>[ofms*ifms*stride_w*k*(fourier_domain_length/2 + 1)];
    kernel_fhoip = new std::complex<KD>[(fourier_domain_length/2 + 1)*k*ofms*ifms*stride_w];
    output_ohf   = new std::complex<T>[ofms*out_h*(fourier_domain_length/2 + 1)]();
    output_fhoy  = new std::complex<T>[k*ofms*img_h*(fourier_domain_length/2 + 1)];
  }

  ~FFTFullGemmRealBenchmark() {
    delete [] image_chf;
    delete [] img_fhcp;
    delete [] kernel_oihf;
    delete [] kernel_fhoip;
    delete [] output_ohf;
    delete [] output_fhoy;
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned ofms, unsigned ifms, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class FFTFullGemmComplexBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride> {
  std::complex<T>  *image_chf;
  std::complex<T>  *img_fhcp;
  std::complex<KD> *kernel_oihf;
  std::complex<KD> *kernel_fhoip;
  std::complex<T>  *output_fhoy;
  std::complex<T>  *output_ohf;

  static constexpr unsigned stride_w = stride;
  static constexpr unsigned stride_h = stride;

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

  static constexpr unsigned fourier_domain_length = triNNity::spectral::fourierDomainLength<img_w, k>();

  public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {}
  virtual void reorganize_output() {}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig* cfg) {}

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, ofms, ifms, img_w, img_h, stride>*>(cfg);

    triNNity::spectral::cpu::transform::image_chw_to_chf_complex<T, img_h, img_w, ifms, k, fourier_domain_length, stride_w>(config->test_image, image_chf);
    triNNity::spectral::cpu::transform::image_chfp_to_fhcp<T, ifms, img_h, fourier_domain_length, stride_w, triNNity::FFT_C2C>(image_chf, img_fhcp);

    triNNity::spectral::cpu::transform::kernel_oihw_to_oihf_complex<KD, k, ofms, ifms, fourier_domain_length, stride_w>(config->kernel, kernel_oihf);
    triNNity::spectral::cpu::transform::kernel_oihfp_to_fohip<KD, ofms, ifms, k, fourier_domain_length, stride_w, triNNity::FFT_C2C>(kernel_oihf, kernel_fhoip);

    triNNity::spectral::convolution_forward_fourier<T, KD, img_w, img_h, ifms, ofms, k, fourier_domain_length,
                                                  triNNity::CONV_MULTI_FFT_FULL_GEMM_COMPLEX, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var,
                                                  (triNNity::layout::spectral_feature_map_layout_t) img_layout,
                                                  (triNNity::layout::spectral_parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::spectral_feature_map_layout_t) out_layout,
                                                  stride, stride>
                                                  (img_fhcp, kernel_fhoip, output_fhoy);

    triNNity::spectral::cpu::transform::out_fhoyp_to_ohfp<T, fourier_domain_length, img_h, k, ofms, stride_w, stride_h, triNNity::FFT_C2C>(output_fhoy, output_ohf);
    triNNity::spectral::cpu::transform::out_mhf_to_mhw_complex<T, img_h, img_w, ofms, k, fourier_domain_length, stride_w, stride_h>(output_ohf, this->output_data);
  }

  virtual std::string name() { return "fft-full-gemm-complex"; }

  FFTFullGemmComplexBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, ofms, ifms, img_w, img_h, stride>() {
    image_chf    = new std::complex<T>[ifms*stride_w*img_h*fourier_domain_length];
    img_fhcp     = new std::complex<T>[fourier_domain_length*img_h*ifms*stride_w];
    kernel_oihf  = new std::complex<KD>[ofms*ifms*stride_w*k*fourier_domain_length];
    kernel_fhoip = new std::complex<KD>[fourier_domain_length*k*ofms*ifms*stride_w];
    output_ohf   = new std::complex<T>[ofms*out_h*fourier_domain_length]();
    output_fhoy  = new std::complex<T>[k*ofms*img_h*fourier_domain_length];
  }

  ~FFTFullGemmComplexBenchmark() {
    delete [] image_chf;
    delete [] img_fhcp;
    delete [] kernel_oihf;
    delete [] kernel_fhoip;
    delete [] output_ohf;
    delete [] output_fhoy;
  }
};
