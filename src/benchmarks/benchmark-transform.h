/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef BENCHMARK_TRANSFORMS_H
#define BENCHMARK_TRANSFORMS_H

#include <algorithm>
#include <iostream>

#include "config.h"
#include "benchmark-utils.h"

template <typename T, unsigned target_channels, unsigned img_w, unsigned img_h>
struct TransformBenchmarkConfig : public benchmark::BenchmarkConfig {

  TransformBenchmarkConfig<T, target_channels, img_w, img_h>(uint64_t r, std::string s)
  : benchmark::BenchmarkConfig(r, s) { }
};

template <typename T, unsigned ifms, unsigned ifm_w, unsigned ifm_h>
class TransformBenchmark : public benchmark::Benchmark {
  triNNity::layout::feature_map_layout_t in_fmt;
  triNNity::layout::feature_map_layout_t out_fmt;

  T *input_buffer, *output_buffer;
public:

  virtual void setup(benchmark::BenchmarkConfig *cfg) {
    input_buffer = new T[ifms*ifm_h*ifm_w];
    output_buffer = new T[ifms*ifm_h*ifm_w];
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    switch (in_fmt) {
      case triNNity::layout::CHW: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            return;
          } break;

          case triNNity::layout::HWC: {
            triNNity::dense::cpu::transform::image_chw_to_hwc<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          case triNNity::layout::HCW: {
            triNNity::dense::cpu::transform::image_chw_to_hcw<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      case triNNity::layout::HWC: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            triNNity::dense::cpu::transform::image_hwc_to_chw<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          case triNNity::layout::HWC: {
            return;
          } break;

          case triNNity::layout::HCW: {
            triNNity::dense::cpu::transform::image_hwc_to_hcw<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      case triNNity::layout::HCW: {
        switch (out_fmt) {
          case triNNity::layout::CHW: {
            triNNity::dense::cpu::transform::image_hcw_to_chw<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          case triNNity::layout::HWC: {
            triNNity::dense::cpu::transform::image_hcw_to_hwc<T, ifms, ifm_h, ifm_w>(this->input_buffer, this->output_buffer);
          } break;

          case triNNity::layout::HCW: {
            return;
          } break;

          default: {
            TRINNITY_ERROR("Not implemented");
          } break;
        }
      } break;

      default: {
        TRINNITY_ERROR("Not implemented");
      } break;
    }
  }

  virtual void teardown(benchmark::BenchmarkConfig *cfg) {
    delete [] input_buffer;
    delete [] output_buffer;
  }

  virtual std::string name() {
    std::string temp = "";
    switch (in_fmt ) {
      case triNNity::layout::CHW: {
        temp += "chw";
      } break;

      case triNNity::layout::HCW: {
        temp += "hcw";
      } break;

      case triNNity::layout::HWC: {
        temp += "hwc";
      } break;
    }

    temp += "-to-";

    switch (out_fmt ) {
      case triNNity::layout::CHW: {
        temp += "chw";
      } break;

      case triNNity::layout::HCW: {
        temp += "hcw";
      } break;

      case triNNity::layout::HWC: {
        temp += "hwc";
      } break;
    }
    return temp;
  }

  TransformBenchmark() { }

  TransformBenchmark(triNNity::layout::feature_map_layout_t i, triNNity::layout::feature_map_layout_t o) : Benchmark() {
    in_fmt = i;
    out_fmt = o;
  }
};

#endif // BENCHMARK_TRANSFORMS_H
