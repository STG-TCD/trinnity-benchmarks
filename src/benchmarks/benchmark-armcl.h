/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#include "config.h"
#include "benchmark.h"

#include <typeinfo>

#include "arm_compute/core/Types.h"
#include "arm_compute/runtime/Tensor.h"
#include "arm_compute/runtime/CPP/CPPScheduler.h"

#if defined(BENCHMARK_ARMCL_NEON)
#include "arm_compute/runtime/NEON/NEFunctions.h"
#include "arm_compute/runtime/NEON/NEScheduler.h"
#endif

#if defined(BENCHMARK_ARMCL_OPENCL)
#include "arm_compute/runtime/CL/CLFunctions.h"
#include "arm_compute/runtime/CL/CLScheduler.h"
#endif

#if defined(BENCHMARK_ARMCL_GLES)
#include "arm_compute/runtime/GLES_COMPUTE/GCFunctions.h"
#include "arm_compute/runtime/GLES_COMPUTE/GCScheduler.h"
#endif

#if defined(BENCHMARK_ARMCL_NEON)

template <typename Conv, typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class ARMCLNEONMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

  arm_compute::CLTensor input, conv1_output, conv1_weights, conv1_biases;
  Conv conv1;

public:

  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    input.allocator()->import_memory(cl::Buffer((uint8_t*) (config->test_image)));
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    conv1_weights.allocator()->import_memory(cl::Buffer((uint8_t*) (config->kernel)));
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    conv1.run();
  }

  virtual std::string name() {
    if (typeid(Conv) == typeid(arm_compute::NEWinogradConvolutionLayer)) {
      return "armcl-neon-winograd";
    } else if (typeid(Conv) == typeid(arm_compute::NEGEMMConvolutionLayer)) {
      return "armcl-neon-patch-gemm";
    } else if (typeid(Conv) == typeid(arm_compute::NEDirectConvolutionLayer)) {
      return "armcl-neon-direct";
    } else {
      TRINNITY_ERROR("Unsupported ARMCL NEON convolution requested");
    }
  }

  ARMCLNEONMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    input.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(img_w, img_h, target_channels), arm_compute::Format::F32));
    conv1_weights.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(k, k, target_channels, kernels), arm_compute::Format::F32));
    conv1_output.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape((triNNity::uceil<img_w, stride>()), (triNNity::uceil<img_h, stride>()), kernels), arm_compute::Format::F32));
    conv1_biases.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(kernels), arm_compute::Format::F32));

    conv1.configure(&input, &conv1_weights, &conv1_biases, &conv1_output, arm_compute::PadStrideInfo(stride, stride, k/2, k/2));

    input.allocator()->allocate();
    conv1_weights.allocator()->allocate();
    conv1_output.allocator()->allocate();
    conv1_biases.allocator()->allocate();

    conv1_weights.map(true);
    conv1_output.map(true);
    conv1_biases.map(true);
    input.map(true);

    conv1_output.allocator()->import_memory(cl::Buffer((uint8_t*) (this->output_data)));

    #if defined(TRINNITY_ARMCL_NUM_THREADS) && (TRINNITY_ARMCL_NUM_THREADS > 1)
    arm_compute::CPPScheduler::get().default_init();
    constexpr unsigned split_dimension = 0;
    arm_compute::CPPScheduler::get().schedule(reinterpret_cast<arm_compute::ICPPKernel*>(conv1), split_dimension);
    #endif
  }
};

#endif // defined(BENCHMARK_ARMCL_NEON)

#if defined(BENCHMARK_ARMCL_OPENCL)

template <typename Conv, typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class ARMCLOPENCLMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

  arm_compute::CLTensor input, conv1_output, conv1_weights, conv1_biases;
  Conv conv1;

public:

  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);

    input.map(true);
    std::memcpy(input.buffer(), config->test_image, input.info()->total_size());
    input.unmap();
    arm_compute::CLScheduler::get().sync();
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);

    conv1_weights.map(true);
    std::memcpy(conv1_weights.buffer(), config->kernel, conv1_weights.info()->total_size());
    conv1_weights.unmap();
    arm_compute::CLScheduler::get().sync();
  }

  virtual void reorganize_output() {
    conv1_output.map(true);
    std::memcpy(this->output_data, conv1_output.buffer(), conv1_output.info()->total_size());
    conv1_output.unmap();
    arm_compute::CLScheduler::get().sync();
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    conv1.run();
    arm_compute::CLScheduler::get().sync();
  }

  virtual std::string name() {
    if (typeid(Conv) == typeid(arm_compute::CLWinogradConvolutionLayer)) {
      return "armcl-opencl-winograd";
    } else if (typeid(Conv) == typeid(arm_compute::CLGEMMConvolutionLayer)) {
      return "armcl-opencl-patch-gemm";
    } else if (typeid(Conv) == typeid(arm_compute::CLDirectConvolutionLayer)) {
      return "armcl-opencl-direct";
    } else {
      TRINNITY_ERROR("Unsupported ARMCL OpenCL convolution requested");
    }
  }

  ARMCLOPENCLMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    input.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(img_w, img_h, target_channels), arm_compute::Format::F32));
    conv1_weights.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(k, k, target_channels, kernels), arm_compute::Format::F32));
    conv1_output.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape((triNNity::uceil<img_w, stride>()), (triNNity::uceil<img_h, stride>()), kernels), arm_compute::Format::F32));
    conv1_biases.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(kernels), arm_compute::Format::F32));

    conv1.configure(&input, &conv1_weights, &conv1_biases, &conv1_output, arm_compute::PadStrideInfo(stride, stride, k/2, k/2));

    input.allocator()->allocate();
    conv1_weights.allocator()->allocate();
    conv1_output.allocator()->allocate();
    conv1_biases.allocator()->allocate();

    #if defined(TRINNITY_ARMCL_NUM_THREADS) && (TRINNITY_ARMCL_NUM_THREADS > 1)
    arm_compute::CLScheduler::get().default_init();
    constexpr unsigned split_dimension = 0;
    arm_compute::CLScheduler::get().schedule(reinterpret_cast<arm_compute::ICPPKernel*>(conv1), split_dimension);
    #endif
  }
};

#endif // defined(BENCHMARK_ARMCL_OPENCL)

#if defined(BENCHMARK_ARMCL_GLES)

template <typename Conv, typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class ARMCLGCMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

  arm_compute::GCTensor input, conv1_output, conv1_weights;
  Conv conv1;

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig * cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);

    arm_compute::GCScheduler::get().default_init();

    input.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(img_w, img_h, target_channels), arm_compute::Format::F32));
    conv1_weights.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape(k, k, target_channels, kernels), arm_compute::Format::F32));
    conv1_output.allocator()->init(arm_compute::TensorInfo(arm_compute::TensorShape((triNNity::uceil<img_w, stride>()), (triNNity::uceil<img_h, stride>()), kernels), arm_compute::Format::F32));

    conv1.configure(&input, &conv1_weights, nullptr, &conv1_output, arm_compute::PadStrideInfo(stride, stride, k/2, k/2));

    input.allocator()->allocate();
    conv1_weights.allocator()->allocate();
    conv1_output.allocator()->allocate();

    // Initialise input and kernel
    input.map(true);
    std::memcpy(input.buffer(), config->test_image, input.info()->total_size());
    conv1_weights.map(true);
    std::memcpy(conv1_weights.buffer(), config->kernel, conv1_weights.info()->total_size());
    conv1_weights.unmap();
    input.unmap();
    conv1.run(); //dummy run
  }

  virtual void reorganize_output() {
    conv1_output.map(true);
    std::memcpy(this->output_data, conv1_output.buffer(), conv1_output.info()->total_size());
    conv1_output.unmap();
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    // Do the convolution
    conv1.run();
  }

  virtual std::string name() {
    if (typeid(Conv) == typeid(arm_compute::GCConvolutionLayer)) {
      return "armcl-gles-gemm";
    } else if (typeid(Conv) == typeid(arm_compute::GCDirectConvolutionLayer)) {
      return "armcl-gles-direct";
    } else {
      TRINNITY_ERROR("Unsupported ARMCL OpenGL convolution requested");
    }
  }

  ARMCLGCMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};

#endif // defined(BENCHMARK_ARMCL_GLES)
