/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#include "config.h"
#include "benchmark.h"

#include <numeric>
#include <triNNity/wrappers/mkldnn/layer.h>

template <triNNity::layer::mkldnn_algo_t wrap_algo, typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class MKLDNNMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

  static constexpr uint32_t batch = 1;

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){}

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);

    try {
      auto cpu_engine = mkldnn::engine(mkldnn::engine::cpu, 0);
      std::vector<mkldnn::primitive> net;
      auto stream = mkldnn::stream(mkldnn::stream::kind::lazy);

      // Set up the input image(s)
      std::vector<float> net_src(batch * target_channels * img_h * img_w);
      T* input_image = config->test_image;
      std::copy(input_image, input_image+(target_channels * img_h * img_w), net_src.begin());

      mkldnn::memory::dims conv_src_tz = {batch, target_channels, img_h, img_w};
      auto conv_user_src_md = mkldnn::memory::desc({conv_src_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);
      auto conv_src_md = mkldnn::memory::desc({conv_src_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);
      auto conv_user_src_memory = mkldnn::memory({conv_user_src_md, cpu_engine}, net_src.data());

      // Set up the kernel
      std::vector<float> conv_weights(kernels * target_channels * k * k);
      KD* input_weights = config->kernel;
      std::copy(input_weights, input_weights+(kernels * target_channels * k * k), conv_weights.begin());

      mkldnn::memory::dims conv_weights_tz = {kernels, target_channels, k, k};
      auto conv_user_weights_md = mkldnn::memory::desc({conv_weights_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::oihw);
      auto conv_weights_md = mkldnn::memory::desc({conv_weights_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::oihw);
      auto conv_user_weights_memory = mkldnn::memory({conv_user_weights_md, cpu_engine}, conv_weights.data());

      // Set up the strides and padding
      mkldnn::memory::dims conv_strides = {stride, stride};
      auto conv_padding = {(int) std::floor(k/2), (int) std::floor(k/2)};

      // Set up the output (we don't allocate storage for this, mkldnn does)
      mkldnn::memory::dims conv_dst_tz = {batch, kernels, (triNNity::uceil<img_h, stride>()), (triNNity::uceil<img_w, stride>())};
      auto conv_dst_md = mkldnn::memory::desc({conv_dst_tz}, mkldnn::memory::data_type::f32, mkldnn::memory::format::nchw);

      mkldnn::algorithm mkldnn_conv_algo;

      switch (wrap_algo) {
        case (triNNity::layer::MKLDNN_DIRECT): {
          mkldnn_conv_algo = mkldnn::convolution_direct;
        } break;

        case (triNNity::layer::MKLDNN_WINOGRAD): {
          mkldnn_conv_algo = mkldnn::convolution_winograd;
        } break;
      }

      /* create a convolution */
      auto conv_desc = mkldnn::convolution_forward::desc(
        mkldnn::prop_kind::forward_inference,
        mkldnn_conv_algo,
        conv_src_md, conv_weights_md, conv_dst_md,
        conv_strides, conv_padding, conv_padding,
        mkldnn::padding_kind::zero);

      auto conv_prim_desc =
          mkldnn::convolution_forward::primitive_desc(conv_desc, cpu_engine);

      /* create reorders between user and data if it is needed and
       *  add it to net before convolution */
      auto conv_src_memory = conv_user_src_memory;
      if (mkldnn::memory::primitive_desc(conv_prim_desc.src_primitive_desc()) !=
          conv_user_src_memory.get_primitive_desc()) {
          conv_src_memory = mkldnn::memory(conv_prim_desc.src_primitive_desc());
          net.push_back(mkldnn::reorder(conv_user_src_memory, conv_src_memory));
      }

      auto conv_weights_memory = conv_user_weights_memory;
      if (mkldnn::memory::primitive_desc(conv_prim_desc.weights_primitive_desc()) !=
          conv_user_weights_memory.get_primitive_desc()) {
          conv_weights_memory = mkldnn::memory(conv_prim_desc.weights_primitive_desc());
          net.push_back(mkldnn::reorder(conv_user_weights_memory, conv_weights_memory));
      }

      auto conv_dst_memory = mkldnn::memory(conv_prim_desc.dst_primitive_desc());

      /* create convolution primitive and add it to net */
      net.push_back(mkldnn::convolution_forward(conv_prim_desc, conv_src_memory, conv_weights_memory, conv_dst_memory));

      stream.submit(net);
      stream.wait();

      std::copy_n((T*)conv_dst_memory.get_data_handle(), (batch * kernels * (triNNity::uceil<img_h, stride>()) * (triNNity::uceil<img_w, stride>())), this->output_data);
    }
    catch(mkldnn::error& e) {
      switch (wrap_algo) {
        case (triNNity::layer::MKLDNN_DIRECT): {
          std::cerr << "MKLDNN ERROR: direct algorithm, status code "
                    << e.status << ", "
                    << e.message << std::endl;
        } break;

        case (triNNity::layer::MKLDNN_WINOGRAD): {
          std::cerr << "MKLDNN ERROR: winograd algorithm, status code "
                    << e.status << ", "
                    << e.message << std::endl;
        } break;
      }
    }
  }

  virtual std::string name() {
    switch (wrap_algo) {
      case (triNNity::layer::MKLDNN_DIRECT): { return "mkldnn-direct"; } break;
      case (triNNity::layer::MKLDNN_WINOGRAD): { return "mkldnn-winograd"; } break;
    }
  }

  MKLDNNMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};
