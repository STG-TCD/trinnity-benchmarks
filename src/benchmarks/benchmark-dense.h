/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#include "config.h"
#include "benchmark.h"

template <typename G, typename T, typename C, typename A, typename KD, unsigned k_w, unsigned k_h, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class SumsMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k_w, k_h, kernels, target_channels, img_w, img_h, stride> {
private:
  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k_w, k_h, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output() {
    if (out_layout == triNNity::layout::HWC) {
      auto input_view = triNNity::internal::memory::View3D<T, out_h, out_w, kernels>(this->output_data_reorganized);
      auto reorganized_view = triNNity::internal::memory::View3D<T, kernels, out_h, out_w>(this->output_data);

      for (unsigned y = 0; y < out_h; y++) {
        for (unsigned x = 0; x < out_w; x++) {
          for (unsigned c = 0; c < kernels; c++) {
            reorganized_view[c][y][x] = input_view[y][x][c];
          }
        }
      }
    }
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k_w, k_h, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    switch (krn_layout) {
      case triNNity::layout::IHWO: {
        this->kernel_reorganized_ckkm = new KD[kernels*target_channels*k_w*k_h];
        triNNity::dense::cpu::transform::kernel_oihw_to_ihwo<KD, k_w, kernels, target_channels>(config->kernel, this->kernel_reorganized_ckkm);
      } break;

      case triNNity::layout::HWIO: {
        this->kernel_reorganized_kkcm = new KD[kernels*target_channels*k_w*k_h];
        triNNity::dense::cpu::transform::kernel_oihw_to_hwio<KD, k_w, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkcm);
      } break;

      case triNNity::layout::OHWI: {
        this->kernel_reorganized_mkkc = new KD[kernels*target_channels*k_w*k_h];
        triNNity::dense::cpu::transform::kernel_oihw_to_ohwi<KD, k_w, kernels, target_channels>(config->kernel, this->kernel_reorganized_mkkc);
      } break;

      default: break;
    }
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k_w, k_h, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    T* localInput  = (img_layout == triNNity::layout::HWC ? this->image_reorganized : config->test_image);
    T* localOutput = (out_layout == triNNity::layout::HWC ? this->output_data_reorganized : this->output_data);
    KD* localKernel;
    switch (krn_layout) {
      case triNNity::layout::IHWO: { localKernel = this->kernel_reorganized_ckkm; } break;
      case triNNity::layout::OIHW: { localKernel = config->kernel; } break;
      case triNNity::layout::HWIO: { localKernel = this->kernel_reorganized_kkcm; } break;
      case triNNity::layout::OHWI: { localKernel = this->kernel_reorganized_mkkc; } break;
      default: { localKernel = nullptr; } break;
    }

    triNNity::dense::convolution_forward_direct<T, A, A, KD, img_w, img_h, target_channels, kernels, k_w, k_h,
                                              triNNity::CONV_MULTI_SUM_SINGLE, CONV_BOUND_METHOD,
                                              (triNNity::layout::feature_map_layout_t) img_layout,
                                              (triNNity::layout::parameter_layout_t)   krn_layout,
                                              (triNNity::layout::feature_map_layout_t) out_layout,
                                              stride, stride>
                                              (localInput, localKernel, localOutput);
  }

  virtual std::string name() {
    if (out_layout == triNNity::layout::HWC) {
      return "direct-sum2d-hwc";
    } else {
      return "direct-sum2d-chw";
    }
  }

  SumsMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k_w, k_h, kernels, target_channels, img_w, img_h, stride>() {
    this->output_data_reorganized = new T[kernels*out_h*out_w];
    this->image_reorganized = new T[img_h * img_w * target_channels];
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr, unsigned patch>
class Im2RowMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {
private:
  std::string benchname;
  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output() {
    if (out_layout == triNNity::layout::HWC) {
      benchmark::transpose2D<T, out_h*out_w, kernels>(this->output_data_reorganized, this->output_data);
    }
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    switch (krn_layout) {
      case triNNity::layout::IHWO: {
        this->kernel_reorganized_ckkm = new KD[kernels*target_channels*k*k];
        triNNity::dense::cpu::transform::kernel_oihw_to_ihwo<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_ckkm);
      } break;

      case triNNity::layout::HWIO: {
        this->kernel_reorganized_kkcm = new KD[kernels*target_channels*k*k];
        triNNity::dense::cpu::transform::kernel_oihw_to_hwio<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkcm);
      } break;

      case triNNity::layout::OHWI: {
        this->kernel_reorganized_mkkc = new KD[kernels*target_channels*k*k];
        triNNity::dense::cpu::transform::kernel_oihw_to_ohwi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_mkkc);
      } break;

      default: break;
    }
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    auto localImage = (img_layout == triNNity::layout::CHW ? config->test_image : this->image_reorganized);
    KD* kernel;
    switch (krn_layout) {
      case triNNity::layout::IHWO: { kernel = this->kernel_reorganized_ckkm; } break;
      case triNNity::layout::OIHW: { kernel = config->kernel; } break;
      case triNNity::layout::HWIO: { kernel = this->kernel_reorganized_kkcm; } break;
      case triNNity::layout::OHWI: { kernel = this->kernel_reorganized_mkkc; } break;
      default: { kernel = nullptr; } break;
    }
    T* localOutput = (out_layout == triNNity::layout::HWC ? this->output_data_reorganized : this->output_data);
    triNNity::dense::convolution_forward_patch_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                                  triNNity::CONV_MULTI_IM2ROW_GEMM, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                                  (triNNity::patch_impl_t) patch,
                                                  (triNNity::layout::feature_map_layout_t) img_layout,
                                                  (triNNity::layout::parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::feature_map_layout_t) out_layout,
                                                  stride, stride,
                                                  gemm_block_i, gemm_block_j, gemm_block_k>
                                                  (localImage, kernel, localOutput);
  }

  virtual std::string name() { return benchname; }

  Im2RowMCMKBenchmark(std::string n) : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    benchname = n;
    this->output_data_reorganized = new T[kernels*out_h*out_w];
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned gemm_block_i, unsigned gemm_block_j, unsigned gemm_block_k, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr, unsigned patch>
class Im2ColMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {
private:
  std::string benchname;
  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){
    if (out_layout == triNNity::layout::HWC) {
      benchmark::transpose2D<T, out_h*out_w, kernels>(this->output_data_reorganized, this->output_data);
    }
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (krn_layout == triNNity::layout::IHWO) {
      this->kernel_reorganized_ckkm = new KD[kernels*target_channels*k*k];
      triNNity::dense::cpu::transform::kernel_oihw_to_ihwo<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_ckkm);
    }
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    KD* kernel = (krn_layout == triNNity::layout::IHWO ? this->kernel_reorganized_ckkm : config->kernel);
    T* localOutput = (out_layout == triNNity::layout::HWC ? this->output_data_reorganized : this->output_data);
    triNNity::dense::convolution_forward_patch_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                                  triNNity::CONV_MULTI_IM2COL_GEMM, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                                  (triNNity::patch_impl_t) patch,
                                                  (triNNity::layout::feature_map_layout_t) img_layout,
                                                  (triNNity::layout::parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::feature_map_layout_t) out_layout,
                                                  stride, stride,
                                                  gemm_block_i, gemm_block_j, gemm_block_k>
                                                  (config->test_image, kernel, localOutput);
  }

  virtual std::string name() { return benchname; }

  Im2ColMCMKBenchmark(std::string n) : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    benchname = n;
    this->output_data_reorganized = new T[kernels*out_h*out_w];
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class K2RASBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_kkmc = new KD[target_channels*k*k*kernels];
    triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkmc);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                            triNNity::CONV_MULTI_KN2ROW_AS_GEMM, CONV_BOUND_METHOD,
                                            (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                            (triNNity::layout::feature_map_layout_t) img_layout,
                                            (triNNity::layout::parameter_layout_t)   krn_layout,
                                            (triNNity::layout::feature_map_layout_t) out_layout>
                                            (config->test_image, this->kernel_reorganized_kkmc, this->output_data);
  }

  virtual std::string name() { return "kn2row-as"; }

  K2RASBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class K2RBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_kkmc = new KD[target_channels*k*k*kernels];
    triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkmc);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                            triNNity::CONV_MULTI_KN2ROW_GEMM, CONV_BOUND_METHOD,
                                            (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                            (triNNity::layout::feature_map_layout_t) img_layout,
                                            (triNNity::layout::parameter_layout_t)   krn_layout,
                                            (triNNity::layout::feature_map_layout_t) out_layout>
                                            (this->image_reorganized, this->kernel_reorganized_kkmc, this->output_data);
  }

  virtual std::string name() { return "kn2row"; }

  K2RBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class K2CASBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_kkmc = new KD[target_channels*k*k*kernels];
    triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkmc);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                            triNNity::CONV_MULTI_KN2COL_AS_GEMM, CONV_BOUND_METHOD,
                                            (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                            (triNNity::layout::feature_map_layout_t) img_layout,
                                            (triNNity::layout::parameter_layout_t)   krn_layout,
                                            (triNNity::layout::feature_map_layout_t) out_layout>
                                            (this->image_reorganized, this->kernel_reorganized_kkmc, this->output_data);
  }

  virtual void teardown(benchmark::BenchmarkConfig *cfg) {
    ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>::teardown(cfg);
  }

  virtual std::string name() { return "kn2col-as"; }

  K2CASBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class K2CBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_ckkm = new KD[kernels*target_channels*k*k];
    triNNity::dense::cpu::transform::kernel_oihw_to_ihwo<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_ckkm);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                            triNNity::CONV_MULTI_KN2COL_GEMM, CONV_BOUND_METHOD,
                                            (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                            (triNNity::layout::feature_map_layout_t) img_layout,
                                            (triNNity::layout::parameter_layout_t)   krn_layout,
                                            (triNNity::layout::feature_map_layout_t) out_layout>
                                            (this->image_reorganized, this->kernel_reorganized_ckkm, this->output_data);
  }

  virtual std::string name() { return "kn2col"; }

  K2CBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr, triNNity::offload_t offload>
class K2RAABenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

private:
  std::string methodName;

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }
  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (krn_layout == triNNity::layout::HWOI) {
      this->kernel_reorganized_kkmc = new KD[target_channels*k*k*kernels];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkmc);
    } else {
      this->kernel_reorganized_kkcm = new KD[target_channels*kernels*k*k];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwio<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkcm);
    }
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    T *tempInput, *tempKernel, *tempOutput;
    tempInput = (img_layout==triNNity::layout::CHW) ? (config->test_image) : (this->image_reorganized);
    tempKernel = (krn_layout==triNNity::layout::HWOI) ? (this->kernel_reorganized_kkmc) : (this->kernel_reorganized_kkcm);
    tempOutput = this->output_data;

    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                            triNNity::CONV_MULTI_KN2ROW_AA_GEMM, CONV_BOUND_METHOD,
                                            (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                            (triNNity::layout::feature_map_layout_t) img_layout,
                                            (triNNity::layout::parameter_layout_t)   krn_layout,
                                            (triNNity::layout::feature_map_layout_t) out_layout>
                                            (tempInput, tempKernel, tempOutput);
  }

  virtual std::string name() { return methodName; }

  K2RAABenchmark(std::string s) : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>(), methodName(s) {}
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class MECRowPartitionBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }
  virtual void reorganize_output(){
    benchmark::transpose2D<T, triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>(), kernels>(this->output_data_reorganized, this->output_data);
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_mkkc = new KD[kernels*target_channels*k*k];
    triNNity::dense::cpu::transform::kernel_oihw_to_ohwi<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_mkkc);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    triNNity::dense::convolution_forward_patch_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                                  triNNity::CONV_MULTI_MEC_ROW_PARTITION, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                                  triNNity::PATCH_ANY,
                                                  (triNNity::layout::feature_map_layout_t) img_layout,
                                                  (triNNity::layout::parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::feature_map_layout_t) out_layout,
                                                  stride, stride>
                                                  (this->image_reorganized, this->kernel_reorganized_mkkc, this->output_data_reorganized);
  }

  virtual std::string name() { return "mec-row-partition"; }

  MECRowPartitionBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    this->output_data_reorganized = new T[kernels*triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>()];
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class MECColBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HCW) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, target_channels, img_w>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][c][x] = input_view[c][y][x];
          }
        }
      }
    }
  }

  virtual void reorganize_output(){
    benchmark::transpose2D<T, triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>(), kernels>(this->output_data_reorganized, this->output_data);
  }

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_reorganized_mkkc = new KD[kernels*target_channels*k*k];
    triNNity::dense::cpu::transform::kernel_oihw_to_ohiw<KD, k, kernels, target_channels>(config->kernel, this->kernel_reorganized_mkkc);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    triNNity::dense::convolution_forward_patch_gemm<T, KD, img_w, img_h, target_channels, kernels, k,
                                                  triNNity::CONV_MULTI_MEC_COL, CONV_BOUND_METHOD,
                                                  (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                                  triNNity::PATCH_ANY,
                                                  (triNNity::layout::feature_map_layout_t) img_layout,
                                                  (triNNity::layout::parameter_layout_t)   krn_layout,
                                                  (triNNity::layout::feature_map_layout_t) out_layout,
                                                  stride, stride>
                                                  (this->image_reorganized, this->kernel_reorganized_mkkc, this->output_data_reorganized);
  }

  virtual std::string name() { return "mec-col"; }

  MECColBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {
    this->output_data_reorganized = new T[kernels*triNNity::uceil<img_w, stride>()*triNNity::uceil<img_h, stride>()];
  }
};

template <typename G, typename T, typename C, typename KD, unsigned gemm_var, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned gemm_transp, unsigned gemm_arr>
class Conv_1x1_GemmMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, 1, 1, kernels, target_channels, img_w, img_h, stride> {
private:
  std::string benchname;
public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, 1, 1, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (img_layout == triNNity::layout::HWC) {

      this->image_reorganized = new T[img_h * img_w * target_channels];
      auto reorganized_view = triNNity::internal::memory::View3D<T, img_h, img_w, target_channels>(this->image_reorganized);
      auto input_view = triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

      for (unsigned y = 0; y < img_h; y++) {
        for (unsigned x = 0; x < img_w; x++) {
          for (unsigned c = 0; c < target_channels; c++) {
            reorganized_view[y][x][c] = input_view[c][y][x];
          }
        }
      }
    }
  }
  virtual void reorganize_output(){
    if (out_layout == triNNity::layout::HWC) {
      benchmark::transpose2D<T, img_w*img_h, kernels>(this->output_data_reorganized, this->output_data);
    }
  }
  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, 1, 1, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    if (krn_layout == triNNity::layout::HWIO) {
      this->kernel_reorganized_kkcm = new KD[kernels*target_channels*1*1];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwio<KD, 1, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkcm);
    } else if (krn_layout == triNNity::layout::HWOI) {
      this->kernel_reorganized_kkmc = new KD[kernels*target_channels*1*1];
      triNNity::dense::cpu::transform::kernel_oihw_to_hwoi<KD, 1, kernels, target_channels>(config->kernel, this->kernel_reorganized_kkmc);
    }
  }
  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, 1, 1, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    auto localImage = (img_layout == triNNity::layout::CHW ? config->test_image : this->image_reorganized);
    KD* kernel;
    switch (krn_layout) {
      case triNNity::layout::HWOI: kernel = this->kernel_reorganized_kkmc; break;
      case triNNity::layout::HWIO: kernel = this->kernel_reorganized_kkcm; break;
      default:  kernel = 0;
    }
    T* localOutput = (out_layout == triNNity::layout::HWC ? this->output_data_reorganized : this->output_data);

    triNNity::dense::convolution_forward_gemm<T, KD, img_w, img_h, target_channels, kernels, 1,
                                              triNNity::CONV_MULTI_CONV_1x1_GEMM, CONV_BOUND_METHOD,
                                              (triNNity::gemm_variant_t) gemm_var, (triNNity::gemm_transpose_t) gemm_transp, (triNNity::gemm_arrangement_t) gemm_arr,
                                              (triNNity::layout::feature_map_layout_t) img_layout,
                                              (triNNity::layout::parameter_layout_t)   krn_layout,
                                              (triNNity::layout::feature_map_layout_t) out_layout>
                                              (localImage, kernel, localOutput);
  }
  virtual std::string name() { return benchname; }
  Conv_1x1_GemmMCMKBenchmark(std::string n) : ComparisonMCMKBenchmark<G, T, KD, C, T, INPUT_BITS, 1, 1, kernels, target_channels, img_w, img_h, stride>(), benchname(n) {
    this->output_data_reorganized = new T[kernels*img_w*img_h];
  }
};
