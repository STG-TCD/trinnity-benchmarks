#include "config.h"
#include "benchmark.h"

#if defined(ENABLE_DAVID_TIMING)
#include <sys/time.h>
#endif

#include <iterator>
#include <type_traits>

template <typename R, unsigned samdWidth>
static constexpr TRINNITY_INLINE R lsbMask() {
  constexpr unsigned lanes = (8*sizeof(R)) / samdWidth;

  R mask = 0;
  R base = 1;

  for (unsigned i = 0; i < lanes; i++) {
    mask |= base;
    mask <<= samdWidth;
  }

  return mask;
}

template <typename R, unsigned samdWidth>
static constexpr TRINNITY_INLINE R msbMask() {
  constexpr unsigned lanes = (8*sizeof(R)) / samdWidth;

  R mask = 0;
  R base = 1 << (samdWidth - 1);

  for (unsigned i = 0; i < lanes; i++) {
    mask |= base;
    mask <<= samdWidth;
  }

  return mask;
}

template <typename R, unsigned samdWidth, unsigned laneIdx>
static constexpr TRINNITY_INLINE R laneMask() {

  R mask = 0;
  R base = (1 << samdWidth) - 1;

  mask |= base;
  mask <<= (samdWidth * laneIdx);

  return mask;
}

template <typename R, unsigned samdWidth>
static TRINNITY_INLINE R add(const R * __restrict__ a, const R * __restrict__ b) {

  constexpr R mask = msbMask<R, samdWidth>();

  R intermediateResult = ((*a) & ~mask) + ((*b) & ~mask);

  R result = intermediateResult ^ (((*a) ^ (*b)) & mask);

  return result;
}

template <typename R, unsigned samdWidth>
static TRINNITY_INLINE R s_add(const R * __restrict__ a, const R * __restrict__ b)
{
   constexpr R mask = msbMask<R, samdWidth>();

   R result = ((*a) & ~mask) + ((*b) & ~mask);

   return result;
}

template <typename R, unsigned samdWidth>
static TRINNITY_INLINE R s_conv_acc(const R * __restrict__ a, const R * __restrict__ b)
{
   constexpr R mask = msbMask<R, samdWidth>();

   R result = ((*a) & ~mask) + (*b);

   return result;
}

template <typename RI, typename RO, unsigned samdWidth>
static TRINNITY_INLINE RO conv(const RI * __restrict__ a, const RI * __restrict__ b) {

  typedef typename std::make_signed<RI>::type SI;

  constexpr RO mask = msbMask<RO, samdWidth>();

  RO a_ro = (RO)((SI)(*a));

  RO b_ro = (RO)((SI)(*b));

  RO product = (a_ro * b_ro);

  RO sign_bits = (product & mask) << 1;

  RO result = add<RO, samdWidth>(&product, &sign_bits);

  return result;
}

template <typename RI, typename RO, unsigned samdWidth>
static TRINNITY_INLINE RO s_conv(const RI * __restrict__ a, const RI * __restrict__ b) {

  typedef typename std::make_signed<RI>::type SI;

  constexpr RI s_mask = msbMask<RI, ((samdWidth-1)/2)+1>();

  RO a_ro = (RO)((SI)(*a));

  RO b_ro = (RO)((SI)(*b));

  RO product = (a_ro * b_ro);

  RO sign_bits = (product & s_mask);

  RO result = product + sign_bits;

  return result;
}

// This function sign extends lane zero ONLY
template <typename R, unsigned samdWidth, unsigned targetWidth>
static TRINNITY_INLINE R sext_lane_0(const R * __restrict__ x) {

  TRINNITY_STATIC_ASSERT(targetWidth >= samdWidth);

  // zero out all bits left of the bits we need to extend
  R clean_x = (*x) & laneMask<R, samdWidth, 0>();

  R msb_x = clean_x & msbMask<R, samdWidth>();

  // Shift the msb left one bit position (by hacks)
  R target_msb = msb_x + msb_x;

  R result = clean_x - target_msb;

  return result;
}

template <typename T, typename printAs, unsigned samdWidth>
static TRINNITY_INLINE void pu(const T x) {
  constexpr unsigned bits = 8*(sizeof(T));
  constexpr uint64_t mask = (1 << samdWidth) - 1;
  constexpr unsigned iterations = bits/(samdWidth) + (bits % (samdWidth) != 0);

  uint64_t lo = x;
  uint64_t hi = x >> (bits/2);

  std::cout << "| ";

  printAs p;

  for(unsigned i = 0; i < iterations/2; i++) {
    p = (printAs)(lo & mask);
    std::cout << p << " | ";
    lo = lo >> samdWidth;
  }

  for(unsigned i = iterations/2; i < iterations; i++) {
    p = (printAs)(hi & mask);
    std::cout << p << " | ";
    hi = hi >> samdWidth;
  }

  std::cout << std::endl;
}

template <typename T, typename printAs, unsigned samdWidth>
static TRINNITY_INLINE void ps(const T x) {
  constexpr unsigned bits = 8*(sizeof(T));
  constexpr uint64_t mask = (1 << samdWidth) - 1;
  constexpr unsigned iterations = bits/(samdWidth) + (bits % (samdWidth) != 0);

  uint64_t lo = x;
  uint64_t hi = x >> (bits/2);

  std::cout << "| ";

  printAs p;

  for(unsigned i = 0; i < iterations/2; i++) {
    uint64_t scratch = lo & mask;
    // now sign-extend this to printAs_bits bits
    sext_lane_0<uint64_t, samdWidth, 8*sizeof(printAs)>(&scratch);
    p = *(reinterpret_cast<printAs*>(&scratch));
    std::cout << p << " | ";
    lo = lo >> samdWidth;
  }

  for(unsigned i = iterations/2; i < iterations; i++) {
    uint64_t scratch = hi & mask;
    // now sign-extend this to printAs_bits bits
    sext_lane_0<uint64_t, samdWidth, 8*sizeof(printAs)>(&scratch);
    p = *(reinterpret_cast<printAs*>(&scratch));
    std::cout << p << " | ";
    hi = hi >> samdWidth;
  }

  std::cout << std::endl;
}

template <typename G, typename T, typename KD, typename A, typename C, unsigned gemm_var, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride, unsigned samdWidth, unsigned output_bits>
class SWARMCMKBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, A, samdWidth-1, k, k, kernels, target_channels, img_w, img_h, stride, output_bits> {
private:

  static constexpr unsigned out_w = triNNity::uceil<img_w, stride>();
  static constexpr unsigned out_h = triNNity::uceil<img_h, stride>();

  typedef typename std::make_unsigned<C>::type UC;

  uint64_t *kernel_cache_mem;

  std::string prefix;

public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}

  virtual void reorganize_output() {}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    this->kernel_cache_mem = build_kernel_cache(cfg);
  }

  // SAMD requires a very specific layout within the machine word, and if the kernel is not already in that layout
  // then there is a significant cost in constructing the machine word each time. This function computes the
  // required layout and caches it in an array
  uint64_t * build_kernel_cache(benchmark::BenchmarkConfig *cfg)
  {
    // boiler plate stuff copied from caller function to set up arrays
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, samdWidth-1, k, k, kernels, target_channels, img_w, img_h, stride, output_bits>*>(cfg);
    auto mikeKernel =  triNNity::internal::memory::View4D<KD, kernels, target_channels, k, k>(config->kernel);

    // rename the template parameters
    signed local_C = target_channels;
    signed local_K = k;
    signed local_M = kernels;

    // get field size from global constant
    #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth;
    #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth+1;
    #else
    #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
    #endif

    // build the cache of pre-processed kernel values
    signed kcache_size = local_M * local_C * local_K;
    uint64_t * kernel_cache_mem = new uint64_t[kcache_size];
    auto kernel_cache = triNNity::internal::memory::View3D<uint64_t, kernels, target_channels, k>(kernel_cache_mem);
    // uint64_t kernel_cache[kernels][target_channels][k];

    // For each kernel in the list
    for (signed m = 0; m < local_M; m++) {
      std::vector<int64_t> values;
      // for each channel(
      for (signed c = 0; c < local_C; c++) {
        // For each row of the kernel
        for (signed kh = 0; kh < local_K; kh++) {
          uint64_t rowBuilder = 0;

          // fill pixel information in the register
          for (signed kw = 0; kw < local_K; kw++) {
            rowBuilder = rowBuilder << (SWARfieldSize*2);
            rowBuilder = rowBuilder + mikeKernel[m][c][kh][local_K - 1 - kw];
          }
          kernel_cache[m][c][kh] = rowBuilder;
        }
      }
    }
    return kernel_cache_mem;
  }

  // similar to the kernel cache, but for the image
  uint64_t* build_image_cache(benchmark::BenchmarkConfig *cfg)
  {
    // boiler plate code to make image accessible
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, samdWidth-1, k, k, kernels, target_channels, img_w, img_h, stride, output_bits>*>(cfg);
    auto mikeImage =  triNNity::internal::memory::View3D<T, target_channels, img_h, img_w>(config->test_image);

    // rename the template parameters
    signed local_H = img_h;
    constexpr signed local_W = img_w;
    signed local_C = target_channels;
    signed local_K = k;

    // get field size from global constant
    #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth;
    #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth+1;
    #else
    #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
    #endif

    constexpr signed imageValuesPerSwarReg = (signed) ( (64 / (2*SWARfieldSize)) +
                                                        ((64 % (2*SWARfieldSize) > SWARfieldSize) ? 1 : 0) );

    constexpr signed double_border = (k/2)*2;
    constexpr signed image_cache_width = triNNity::uceil<local_W+double_border, imageValuesPerSwarReg>();

    // allocate the image cache
    signed icache_size = local_C * (local_H+double_border) * image_cache_width;
    uint64_t * image_cache_mem = new uint64_t[icache_size];
    auto image_cache =  triNNity::internal::memory::View3D<uint64_t, img_h+double_border, image_cache_width, target_channels>(image_cache_mem);

    #ifdef INSTRUMENT_CACHE
    for (signed t = 0; t < icache_size; t++) {
      image_cache_mem[t] = 0xDEADBEEF;
    }
    #endif // INSTRUMENT CACHE

    // build the image cache
    for (signed c = 0; c < local_C; c++) {
      std::vector<int64_t> values;
      for (signed h = -(local_K/2); h < local_H + (local_K/2); h++) {
        for (signed w = -(local_K/2); w < local_W; w+= (signed)imageValuesPerSwarReg) {
          uint64_t rowBuilder = 0;
          // fill pixel information in the register
          for (signed kw = 0; kw < (signed) imageValuesPerSwarReg; kw++) {
            rowBuilder = rowBuilder << (SWARfieldSize*2);
            // Fill zeroes if no image data left -> always same offsets in SWAR reg for products
            if (w+kw < 0 || w + kw >= local_W || h < 0 || h >= local_H) {}
            else {
              // We still have real image data
              rowBuilder = rowBuilder + mikeImage[c][h][w+kw];
            }
          }
          image_cache[h+local_K/2][((w+(local_K/2))/imageValuesPerSwarReg)][c] = rowBuilder;
        } // for w
      } // for h
    } // for c
    return image_cache_mem;
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {

    #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth;
    #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
    constexpr unsigned SWARfieldSize = samdWidth+1;
    #else
    #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
    #endif

    auto mikeOutput = triNNity::internal::memory::View3D<A, kernels, img_h, img_w>(this->output_data);

    // Create mask for extracting rhombus partials
    unsigned __int128 maskLower;
    maskLower = pow(2, 4*SWARfieldSize)-1;

    constexpr unsigned imageValuesPerSwarReg = (size_t) ( (64 / (2*SWARfieldSize)) +
                                                          ((64 % (2*SWARfieldSize) > SWARfieldSize) ? 1 : 0) );

    std::size_t completeValuesPerRhombus = imageValuesPerSwarReg - k + 1;
    std::size_t valuesPerConvolution = imageValuesPerSwarReg + k-1;

    // create signed versions of key parameters
    const signed local_K = k;
    const signed local_H = img_h;
    const signed local_W = img_w;
    const signed local_M = kernels;
    const signed local_C = target_channels;

    constexpr signed double_border = (k/2)*2;
    constexpr signed image_cache_width = triNNity::uceil<local_W+double_border, imageValuesPerSwarReg>();

    #if defined(ENABLE_DAVID_TIMING)
    #define TIMING(_x) _x
    #else
    #define TIMING(_x)
    #endif

    TIMING(struct timeval start_time);
    TIMING(struct timeval stop_time);
    /* record starting time */
    TIMING(gettimeofday(&start_time, NULL));

    uint64_t * image_cache_mem = build_image_cache(cfg);
    auto image_cache =  triNNity::internal::memory::View3D<uint64_t, local_H+double_border, image_cache_width, local_C>(image_cache_mem);

    /* record finishing time */
    TIMING(gettimeofday(&stop_time, NULL));
    TIMING(long cache_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L + (stop_time.tv_usec - start_time.tv_usec));
    TIMING(std::cout << "Image caching time in microseconds " << cache_time << endl);

    /* record starting time */
    TIMING(gettimeofday(&start_time, NULL));

    auto kernel_cache = triNNity::internal::memory::View3D<uint64_t, local_M, local_C, local_K>(this->kernel_cache_mem);

    /* record finishing time */
    TIMING(gettimeofday(&stop_time, NULL));
    TIMING(cache_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L + (stop_time.tv_usec - start_time.tv_usec));
    TIMING(std::cout << "Kernel caching time in microseconds " << cache_time << endl);

    /* record starting time */
    TIMING(gettimeofday(&start_time, NULL));

    //#define RESULT_CACHING
#ifdef RESULT_CACHING
    unsigned __int128 * result_cache_mem = convolve_caches(image_cache_mem, kernel_cache_mem);
    const signed w_range = local_W + double_border;
    auto result_cache = triNNity::internal::memory::View3D<unsigned __int128, local_M, local_H, w_range>(result_cache_mem);
#endif
    /* record finishing time */
    TIMING(gettimeofday(&stop_time, NULL));
    TIMING(cache_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L + (stop_time.tv_usec - start_time.tv_usec));
    TIMING(std::cout << "Result caching time in microseconds " << cache_time << endl);


    //#define WRITEOUT_CACHE
#ifdef WRITEOUT_CACHE
    write_back_result(result_cache_mem);
    return;
#endif

    /* record starting time of convolution */
    TIMING(gettimeofday(&start_time, NULL));

    // now do the MCMK convolution

    // For each kernel in the list
    for (signed m = 0; m < local_M; m++) {
      // Move down image by one row at a time
      for (signed h = 0; h < local_H; h += 2) {
        unsigned __int128 nextOverlap0 = 0;
        unsigned __int128 nextOverlap1 = 0;

        // Move across image in blocks that will fit in SWAR register, taking into account added zeros
        // for 1D convolution overflow (halves the number that will fit)
        for (signed w = -(local_K/2); w < local_W + (local_K/2); w+= (signed)imageValuesPerSwarReg) {
          unsigned __int128 totalConvolutionResult0, totalConvolutionResult1;

          totalConvolutionResult0 = 0;
          totalConvolutionResult1 = 0;

          // Iterate over each channel
          for (signed c = 0; c < local_C; c++) {
            // For each row of the kernel
            for (signed kh = 0; kh < local_K; kh++) {

              uint64_t imageRow0, imageRow1, kernelRow;

              imageRow0 = image_cache[h+kh][((w+(local_K/2))/imageValuesPerSwarReg)][c];
              imageRow1 = image_cache[h+1+kh][((w+(local_K/2))/imageValuesPerSwarReg)][c];

              kernelRow = kernel_cache[m][c][kh];

              unsigned __int128 fastConvResult0, fastTotal0;
              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(imageRow0);
              ps<unsigned __int128, C, 2*SWARfieldSize>(kernelRow);
              #endif

              #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
              fastConvResult0 = conv<uint64_t, unsigned __int128, 2*SWARfieldSize>(&imageRow0, &kernelRow);
              #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
              fastConvResult0 = s_conv<uint64_t, unsigned __int128, 2*SWARfieldSize>(&imageRow0, &kernelRow);
              #else
              #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
              #endif

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              ps<unsigned __int128, C, 2*SWARfieldSize>(fastConvResult0);
              ps<unsigned __int128, C, 2*SWARfieldSize>(totalConvolutionResult0);
              #endif

              #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
              fastTotal0 = add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult0, &fastConvResult0);
              #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
              fastTotal0 = s_conv_acc<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult0, &fastConvResult0);
              #else
              #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
              #endif

              totalConvolutionResult0 = fastTotal0;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              ps<unsigned __int128, C, 2*SWARfieldSize>(totalConvolutionResult0);
              #endif

              unsigned __int128 fastConvResult1, fastTotal1;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(imageRow1);
              ps<unsigned __int128, C, 2*SWARfieldSize>(kernelRow);
              #endif

              #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
              fastConvResult1 = conv<uint64_t, unsigned __int128, 2*SWARfieldSize>(&imageRow1, &kernelRow);
              #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
              fastConvResult1 = s_conv<uint64_t, unsigned __int128, 2*SWARfieldSize>(&imageRow1, &kernelRow);
              #else
              #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
              #endif

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              ps<unsigned __int128, C, 2*SWARfieldSize>(fastConvResult1);
              ps<unsigned __int128, C, 2*SWARfieldSize>(totalConvolutionResult1);
              #endif

              #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
              fastTotal1 = add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult1, &fastConvResult1);
              #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
              fastTotal1 = s_conv_acc<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult1, &fastConvResult1);
              #else
              #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
              #endif

              totalConvolutionResult1 = fastTotal1;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              ps<unsigned __int128, C, 2*SWARfieldSize>(totalConvolutionResult1);
              #endif

            } // for kh < local_K
          } // for c < local_C


          // At this point, we have a new completed Rhombus for multiple channels
          // Resolve partials, and write new full results to output

          // If we we are at the beginning of image row -> no need to add initial partials
          if (w == -(local_K/2)) {
            // Dont write incomplete results from beginning of rhombus -> just write complete values
            for (signed l = 0; (l < (signed)completeValuesPerRhombus) && ((w+k/2+l) < img_w); l++) {
              unsigned __int128 result0 = totalConvolutionResult0 >> (2*SWARfieldSize * (imageValuesPerSwarReg-l-1));
              //result0 = result0 & ((1 << (2*SWARfieldSize))-1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl << "writing lane 0 of: " << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(result0);
              #endif

              uint64_t extendedResult0 = result0;
              sext_lane_0<uint64_t, 2*SWARfieldSize, std::max<unsigned>(2*SWARfieldSize, 8*sizeof(C))>(&extendedResult0);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "after sign extension, register is: " << std::endl;
              ps<uint64_t, C, 8*sizeof(T)>(extendedResult0);
              #endif

              UC bits0 = extendedResult0; // extract the low bit, unsigned
              C *bitPattern0 = reinterpret_cast<C*>(&bits0); // reinterpret bit pattern as signed
              mikeOutput[m][h][w+local_K/2+l] = *bitPattern0;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "written as: " << mikeOutput[m][h][w+local_K/2+l] << std::endl;
              #endif

              unsigned __int128 result1 = totalConvolutionResult1 >> (2*SWARfieldSize * (imageValuesPerSwarReg-l-1));
              //result1 = result1 & ((1 << (2*SWARfieldSize))-1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl << "writing lane 0 of: " << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(result1);
              #endif

              uint64_t extendedResult1 = result1;
              sext_lane_0<uint64_t, 2*SWARfieldSize, std::max<unsigned>(2*SWARfieldSize, 8*sizeof(C))>(&extendedResult1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "after sign extension, register is: " << std::endl;
              ps<uint64_t, C, 8*sizeof(T)>(extendedResult1);
              #endif

              UC bits1 = extendedResult1; // extract the low bit, unsigned
              C *bitPattern1 = reinterpret_cast<C*>(&bits1); // reinterpret bit pattern as signed
              mikeOutput[m][h+1][w+local_K/2+l] = *bitPattern1;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "written as: " << mikeOutput[m][h+1][w+local_K/2+l] << std::endl;
              #endif
            }
          }
          else {
            // We are not at the beginning of a row -> add partials from previous and current rhombus
            // as well as new complete values from this convolution round

            // Add previous overlap
            #if defined(BENCHMARK_SAMD_TEMPORARY_SPACERS)
            totalConvolutionResult0 = add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult0, &nextOverlap0);
            totalConvolutionResult1 = add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult1, &nextOverlap1);
            #elif defined(BENCHMARK_SAMD_PERMANENT_SPACERS)
            totalConvolutionResult0 = s_add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult0, &nextOverlap0);
            totalConvolutionResult1 = s_add<unsigned __int128, 2*SWARfieldSize>(&totalConvolutionResult1, &nextOverlap1);
            #else
            #error "Must define either BENCHMARK_SAMD_TEMPORARY_SPACERS or BENCHMARK_SAMD_PERMANENT_SPACERS in SAMD benchmarking"
            #endif

            for (signed l = 0; l < (signed)imageValuesPerSwarReg && w - local_K + 2 + l < local_W; l++) {
              unsigned __int128 result0 = totalConvolutionResult0 >> (2*SWARfieldSize * (valuesPerConvolution-l-1));
              //result0 = result0 & ((1 << (2*SWARfieldSize))-1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl << "writing lane 0 of: " << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(result0);
              #endif

              uint64_t extendedResult0 = result0;
              sext_lane_0<uint64_t, 2*SWARfieldSize, std::max<unsigned>(2*SWARfieldSize, 8*sizeof(C))>(&extendedResult0);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "after sign extension, register is: " << std::endl;
              ps<uint64_t, C, 8*sizeof(T)>(extendedResult0);
              #endif

              UC bits0 = extendedResult0; // extract the low bits, unsigned
              C *bitPattern0 = reinterpret_cast<C*>(&bits0); // reinterpret bit pattern as signed
              mikeOutput[m][h][w - local_K + 2 + l] = *bitPattern0;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "written as: " << mikeOutput[m][h][w - local_K + 2 + l] << std::endl;
              #endif

              unsigned __int128 result1 = totalConvolutionResult1 >> (2*SWARfieldSize * (valuesPerConvolution-l-1));
              //result1 = result1 & ((1 << (2*SWARfieldSize))-1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << std::endl << "writing lane 0 of: " << std::endl;
              ps<unsigned __int128, C, 2*SWARfieldSize>(result1);
              #endif

              uint64_t extendedResult1 = result1;
              sext_lane_0<uint64_t, 2*SWARfieldSize, std::max<unsigned>(2*SWARfieldSize, 8*sizeof(C))>(&extendedResult1);

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "after sign extension, register is: " << std::endl;
              ps<uint64_t, C, 8*sizeof(T)>(extendedResult1);
              #endif

              UC bits1 = extendedResult1; // extract the low bit, unsigned
              C *bitPattern1 = reinterpret_cast<C*>(&bits1); // reinterpret bit pattern as signed
              mikeOutput[m][h+1][w - local_K + 2 + l] = *bitPattern1;

              #if defined(BENCHMARK_SWAR_DUMP_REGISTERS)
              std::cout << "written as: " << mikeOutput[m][h+1][w - local_K + 2 + l] << std::endl;
              #endif
            }
          }

          // Create the overlap to add to the next rhombus.
          nextOverlap0 = (totalConvolutionResult0 & maskLower) << (imageValuesPerSwarReg * 2 * SWARfieldSize);
          nextOverlap1 = (totalConvolutionResult1 & maskLower) << (imageValuesPerSwarReg * 2 * SWARfieldSize);
        }
      } // for h < local_H, h += 2
    } // for m < local_M

    /* record finishing time */
    TIMING(gettimeofday(&stop_time, NULL));
    TIMING(long mul_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L + (stop_time.tv_usec - start_time.tv_usec));
    TIMING(std::cout << "Conv time: %lld microseconds " << mul_time << endl);

#ifdef INSTRUMENT_CACHE
    std::cerr << "img cache incorrect: " << img_cache_incorrect << std::endl;
    std::cerr << "img cache uninit: " << img_cache_uninitialized << std::endl;
    std::cout << "img cache correct: " << img_cache_correct << std::endl;
#endif
  }

  virtual std::string name() { return "samd"+std::to_string(samdWidth); }

  SWARMCMKBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, A, samdWidth-1, k, k, kernels, target_channels, img_w, img_h, stride, output_bits>() {}
  ~SWARMCMKBenchmark() {}
};

