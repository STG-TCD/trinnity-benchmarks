/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#include "config.h"
#include "benchmark.h"

template <typename G, typename T, typename C, typename A, typename KD, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class SparseMHWBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {
  const triNNity::sparse::cpu::primitive::sparse_kernel<KD> *kernel_sliced;
public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){}

  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_sliced = triNNity::sparse::cpu::primitive::create_packed_oihw<KD, target_channels, kernels, k>(config->kernel);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    triNNity::sparse::cpu::impl::sparse_loop_mhw<T, KD, img_w, img_h, target_channels, kernels, k,
                                                 CONV_BOUND_METHOD,
                                                 stride, stride>
                                                 (config->test_image, this->kernel_sliced, this->output_data);
  }

  virtual std::string name() { return "sparse-mhw"; }

  SparseMHWBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}

  ~SparseMHWBenchmark() {
    triNNity::sparse::cpu::primitive::destroy_packed_oihw(this->kernel_sliced);
  }
};

template <typename G, typename T, typename C, typename A, typename KD, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class SparseMHWWBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {
  const triNNity::sparse::cpu::primitive::sparse_kernel<KD> *kernel_sliced;
public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_sliced = triNNity::sparse::cpu::primitive::create_packed_oihw<KD, target_channels, kernels, k>(config->kernel);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    triNNity::sparse::cpu::impl::sparse_loop_mhw_unroll_w<T, KD, img_w, img_h, target_channels, kernels, k,
                                                          CONV_BOUND_METHOD,
                                                          stride, stride>
                                                          (config->test_image, this->kernel_sliced, this->output_data);
  }

  virtual std::string name() { return "sparse-mhw-w"; }

  SparseMHWWBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}

  ~SparseMHWWBenchmark() {
    triNNity::sparse::cpu::primitive::destroy_packed_oihw(this->kernel_sliced);
  }
};

template <typename G, typename T, typename C, typename A, typename KD, unsigned k, unsigned kernels, unsigned target_channels, unsigned img_w, unsigned img_h, unsigned img_layout, unsigned krn_layout, unsigned out_layout, unsigned stride>
class SparseMHWMBenchmark : public ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride> {
  const triNNity::sparse::cpu::primitive::sparse_kernel<KD> *kernel_sliced;
public:
  virtual void reorganize_image(benchmark::BenchmarkConfig *cfg){}
  virtual void reorganize_output(){}
  virtual void reorganize_kernel(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    this->kernel_sliced = triNNity::sparse::cpu::primitive::create_packed_oihw<KD, target_channels, kernels, k>(config->kernel);
  }

  virtual void benchmark(benchmark::BenchmarkConfig *cfg) {
    auto config = reinterpret_cast<MCMKBenchmarkConfig<G, T, KD, C, k, k, kernels, target_channels, img_w, img_h, stride>*>(cfg);
    triNNity::sparse::cpu::impl::sparse_loop_mhw_unroll_m<T, KD, img_w, img_h, target_channels, kernels, k,
                                                          CONV_BOUND_METHOD,
                                                          stride, stride>
                                                          (config->test_image, this->kernel_sliced, this->output_data);
  }

  virtual std::string name() { return "sparse-mhw-m"; }

  SparseMHWMBenchmark() : ComparisonMCMKBenchmark<G, T, KD, C, A, INPUT_BITS, k, k, kernels, target_channels, img_w, img_h, stride>() {}

  ~SparseMHWMBenchmark() {
    triNNity::sparse::cpu::primitive::destroy_packed_oihw(this->kernel_sliced);
  }
};
