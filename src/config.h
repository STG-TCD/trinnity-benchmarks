/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef BENCHMARK_CONFIG_H
#define BENCHMARK_CONFIG_H

#if !defined(TRINNITY_CBLAS_ROW_MAJOR) && !defined(TRINNITY_CBLAS_COLUMN_MAJOR)
#define TRINNITY_CBLAS_ROW_MAJOR
#endif

#if defined(BENCHMARK_CBLAS)
#define TRINNITY_USE_CBLAS_GEMM
#define TRINNITY_USE_CBLAS_GEMV
#elif defined(BENCHMARK_MKL)
#define TRINNITY_USE_MKL_GEMM
#define TRINNITY_USE_MKL_GEMV
#elif defined(BENCHMARK_ARMCL)
#define TRINNITY_USE_ARMCL_GEMM
#define TRINNITY_USE_CBLAS_GEMV
#elif defined(BENCHMARK_CUBLAS)
#define TRINNITY_USE_CUBLAS_GEMM
#elif defined(BENCHMARK_CLBLAS)
#define TRINNITY_USE_CLBLAS_GEMM
#define TRINNITY_USE_CBLAS_GEMV
#elif defined(BENCHMARK_CLBLAST)
#define TRINNITY_USE_CLBLAST_GEMM
#define TRINNITY_USE_CBLAS_GEMV
#endif

#if defined(BENCHMARK_FFTW)
#define TRINNITY_USE_FFTW
#endif

// Do the multiplies in accumulate precision
// (high-precision mode)
#define TRINNITY_DIRECT_MUL_PRECISION_A

// These includes must come after we define the TRINNITY_ parameters

#if defined(BENCHMARK_MKLDNN)
#define TRINNITY_WRAP_MKLDNN
#include <triNNity/wrappers/mkldnn/layer.h>
#endif

#if defined(BENCHMARK_ARMCL)
#define TRINNITY_WRAP_ARMCL
#include <triNNity/wrappers/armcl/layer.h>
#endif

#include <triNNity/config.h>
#include <triNNity/dense/cpu/memory.h>
#include <triNNity/dense/cpu/mcmk.h>
#include <triNNity/dense/cpu/shape.h>

#if defined(BENCHMARK_SPECTRAL)
#include <triNNity/spectral/cpu/config.h>
#include <triNNity/spectral/cpu/shape.h>
#include <triNNity/spectral/cpu/mcmk.h>
#endif

#if defined(BENCHMARK_SPARSE)
#include <triNNity/sparse/cpu/config.h>
#include <triNNity/sparse/cpu/impl.h>
#endif

// Whether to use the interior (non-boundary) pixels
// for error checks, or do full checking (with boundary)
// or just use the boundary pixels, can use 0 or more at once

#if !defined(BENCHMARK_ERROR_FULL) && !defined(BENCHMARK_ERROR_INTERIOR)
#define BENCHMARK_ERROR_FULL
#endif

// What boundary pixel handling method to use in convolution
// Needs to be in sync with the error checking specified above
#define CONV_BOUND_METHOD triNNity::BOUND_IMPLICIT_PAD

#if !defined(GROUNDTRUTH_IMAGE_TYPE)
#define GROUNDTRUTH_IMAGE_TYPE double
#endif

#if !defined(IMAGE_TYPE)
#define IMAGE_TYPE float
#endif

#if !defined(GROUNDTRUTH_ACCUM_TYPE)
#define GROUNDTRUTH_ACCUM_TYPE double
#endif

#if !defined(ACCUM_TYPE)
#define ACCUM_TYPE float
#endif

#if !defined(KERNEL_TYPE)
#define KERNEL_TYPE float
#endif

#if !defined(CORRECTNESS_CHECK_TYPE)
#define CORRECTNESS_CHECK_TYPE double
#endif

#if !defined(WHAT_GEMM)
#if defined(BENCHMARK_CBLAS) || defined(BENCHMARK_CUBLAS) || defined(BENCHMARK_CLBLAS) || defined(BENCHMARK_CLBLAST) || defined(BENCHMARK_MKL) || defined(BENCHMARK_FFTW)
#define WHAT_GEMM triNNity::GEMM_BLAS
#else
#define WHAT_GEMM triNNity::GEMM_BLOCKED
#endif
#endif

#if !defined(BENCHMARK_GEMM_BLOCK_I)
#define BENCHMARK_GEMM_BLOCK_I 4
#endif

#if !defined(BENCHMARK_GEMM_BLOCK_J)
#define BENCHMARK_GEMM_BLOCK_J 1
#endif

#if !defined(BENCHMARK_GEMM_BLOCK_K)
#define BENCHMARK_GEMM_BLOCK_K 4
#endif

#if !defined(WHAT_GEMV)
#if defined(BENCHMARK_CBLAS) || defined(BENCHMARK_CUBLAS) || defined(BENCHMARK_CLBLAS) || defined(BENCHMARK_CLBLAST) || defined(BENCHMARK_MKL) || defined(BENCHMARK_FFTW)
#define WHAT_GEMV triNNity::GEMV_BLAS
#else
#define WHAT_GEMV triNNity::GEMV_SIMPLE
#endif
#endif

#if !defined(KERNEL_VALUE_RANGE)
#define KERNEL_VALUE_RANGE 8
#endif

#if !defined(IMAGE_VALUE_RANGE)
#define IMAGE_VALUE_RANGE 128
#endif

#if !defined(INPUT_BITS)
#define INPUT_BITS 8
#else
#define ENFORCE_INPUT_BITS
#endif

#if !defined(OUTPUT_BITS)
#define OUTPUT_BITS 8
#else
#define ENFORCE_OUTPUT_BITS
#endif

#if !defined(WINOGRAD_MEASURE_KERNEL_TRANSFORM)
#define WINOGRAD_MEASURE_KERNEL_TRANSFORM true
#endif

#endif // BENCHMARK_CONFIG_H
