/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef BENCHMARK_UTILS_H
#define BENCHMARK_UTILS_H

#define BENCHMARK_ERROR(x) throw std::runtime_error(x);

#if defined(BENCHMARK_PLATFORM_X64)

static inline uint64_t __attribute__((always_inline)) clocks() {
    uint64_t a, d;
    __volatile__ uint64_t x = 0, y = 0;

    __asm__ __volatile__ ( "cpuid"
                     : "=a" (y)
                     : "0" (x)
                     : "%rbx", "%rcx", "%rdx", "memory");

    __asm__ __volatile__ ("rdtsc"
                     : "=a" (a), "=d" (d)
                     :
                     : "memory");

    return (a | d << 32);
}

static inline uint64_t __attribute__((always_inline)) clocks_noserialize() {
    uint64_t a, d;

    __asm__  ("rdtsc"
             : "=a" (a), "=d" (d)
             : );

    return (a | d << 32);
}

#if defined(USE_PCM)
#include <cpucounters.h>
#elif defined(USE_PAPI)
#include <papi.h>
#define NUM_EVENTS 4
#elif defined(USE_RAPL)
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <asm/unistd.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>

#define NUM_RAPL_DOMAINS 4

char rapl_domain_names[NUM_RAPL_DOMAINS][30]= {
  "energy-cores",
  "energy-gpu",
  "energy-pkg",
  "energy-ram"
};

#define BUFSIZ 256
#define MAX_CPUS 1024
#define MAX_PACKAGES 16

static int package_map[MAX_PACKAGES];

static int total_packages = 0;
static int total_cores = 0;

static int detect_packages(void) {
  char filename[BUFSIZ];
  FILE *fff;
  int package;
  int i;

  for(i=0;i<MAX_PACKAGES;i++) { package_map[i]=-1; }

  for(i=0;i<MAX_CPUS;i++) {
    sprintf(filename,"/sys/devices/system/cpu/cpu%d/topology/physical_package_id",i);
    fff=fopen(filename,"r");
    if (fff==NULL) break;
    fscanf(fff,"%d",&package);
    fclose(fff);

    if (package_map[package]==-1) {
      total_packages++;
      package_map[package]=i;
    }
  }

  total_cores=i;
  return 0;
}

long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                     int cpu, int group_fd, unsigned long flags) {
  int ret = syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
  return ret;
}

#endif
#endif

#if defined(BENCHMARK_PLATFORM_ARM)

#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>

long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                     int cpu, int group_fd, unsigned long flags) {
  int ret = syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
  return ret;
}

#endif

#if defined(ANALOG_DISCOVERY_2_POWER_MEASUREMENTS)
#include <digilent/waveforms/dwf.h>
#define AD_CHANNEL_1 0
#define AD_CHANNEL_2 1
#endif

#if defined(BENCHMARK_ODROID_GPIO)
#include <sys/stat.h>
#include <fcntl.h>
#if !defined(TRIGGER_PIN)
  #define TRIGGER_PIN 23
#endif
#if !defined(TRIGGER_PIN_2)
  #define TRIGGER_PIN_2 33
#endif
#endif

#include <cstdint>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <iomanip>


namespace benchmark {

template <typename T, unsigned H, unsigned W>
void transpose2D(const T* __restrict__ in, T* __restrict__ out) {
  for (unsigned h = 0; h < H; h++) {
    for (unsigned w = 0; w < W; w++) {
      out[w*H+h] = in[h*W+w];
    }
  }
}

template <typename T>
static TRINNITY_INLINE void print2D(const size_t w, const size_t h, const T *in) {
  std::cout << std::endl;
  for (size_t i = 0; i < h; i++) {
    for (size_t j = 0; j < w; j++) {
      std::cout << +in[i*w+j] << " " ;
    }
    std::cout << std::endl;
  }
}

struct BenchmarkConfig {
  uint64_t runs;
  std::string resultsdir;

  virtual void makeResultsFile(std::ofstream& ofs, const char *id, const char *statName) {
    char mkcmd[256];
    sprintf(mkcmd, "mkdir -p %s/%s/", this->resultsdir.c_str(), id);
    if (0 != system(mkcmd)) {
      BENCHMARK_ERROR("Could not create directory for results! (tried: " + std::string(mkcmd) + ")");
    } else {
      char path[256];
      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, statName);
      ofs.open(path, std::ofstream::out);
    }
  }

  virtual void wipeResultsFile(const char *id) {
    char mkcmd[256];
    sprintf(mkcmd, "mkdir -p %s/%s/", this->resultsdir.c_str(), id);
    if (0 != system(mkcmd)) {
      BENCHMARK_ERROR("Could not create directory for results! (tried: " + std::string(mkcmd) + ")");
    } else {
      std::ofstream ofs;
      char path[256];
      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "hcy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "hns");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "ipc");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "l2m");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "l2s");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "l3m");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "l3s");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "xfr");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "ret");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "dram-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "dram-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "package-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "package-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "dram-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "dram-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "package-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "package-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "core-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "core-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "gpu-energy");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();

      sprintf(path, "%s/%s/%s.dat",  this->resultsdir.c_str(), id, "gpu-counts");
      ofs.open(path, std::ofstream::out | std::ofstream::trunc);
      ofs.close();
    }
  }

  BenchmarkConfig(uint64_t r, std::string s)
  : runs(r), resultsdir(s) {}

  virtual ~BenchmarkConfig(){}
};

struct Measurements {
  uint64_t *hostCycles, *l2Misses, *l2StallCycles, *l3Misses, *l3StallCycles, *bytesRead, *instsRetired;
  int64_t *dramCounts, *packageCounts, *coreCounts, *gpuCounts;
  double *dramEnergy, *packageEnergy, *coreEnergy, *gpuEnergy;
  double *instsPerClock;
  double *hostTimes, *deviceTimes;

  Measurements(const uint64_t runs) {
    hostCycles = new uint64_t[runs];
    hostTimes = new double[runs];
    deviceTimes = new double[runs];
    instsPerClock = new double[runs];
    l2Misses = new uint64_t[runs];
    l2StallCycles = new uint64_t[runs];
    l3Misses = new uint64_t[runs];
    l3StallCycles = new uint64_t[runs];
    bytesRead = new uint64_t[runs];
    instsRetired = new uint64_t[runs];
    dramCounts = new int64_t[runs];
    dramEnergy = new double[runs];
    packageCounts = new int64_t[runs];
    packageEnergy = new double[runs];
    coreCounts = new int64_t[runs];
    coreEnergy = new double[runs];
    gpuCounts = new int64_t[runs];
    gpuEnergy = new double[runs];
  }

  ~Measurements() {
    delete [] hostCycles;
    delete [] hostTimes;
    delete [] deviceTimes;
    delete [] instsPerClock;
    delete [] l2Misses;
    delete [] l2StallCycles;
    delete [] l3Misses;
    delete [] l3StallCycles;
    delete [] bytesRead;
    delete [] instsRetired;
    delete [] dramCounts;
    delete [] dramEnergy;
    delete [] packageCounts;
    delete [] packageEnergy;
    delete [] coreCounts;
    delete [] coreEnergy;
    delete [] gpuCounts;
    delete [] gpuEnergy;
  }
};

class Benchmark {
protected:

  #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
  PCM *pcm_handle;
  #endif

public:
  Measurements *data;

  virtual void setup(BenchmarkConfig*) = 0;
  virtual void benchmark(BenchmarkConfig*) = 0;
  virtual void teardown(BenchmarkConfig*) = 0;
  virtual std::string name() = 0;

  Benchmark() {}
  virtual ~Benchmark() {}

  void run(BenchmarkConfig *cfg) {

    #if defined(ANALOG_DISCOVERY_2_POWER_MEASUREMENTS)
    HDWF hdwf;
    STS sts;
    double* channel1Samples;
    double* channel2Samples;
    int cSamples;
    double hzAcq = 5000000.000;
    char szError[512] = {0};

    if(!FDwfDeviceOpen(-1, &hdwf)) {
        FDwfGetLastErrorMsg(szError);
        printf("Device open failed\n\t%s", szError);
    }


    // enable channels
    FDwfAnalogInChannelEnableSet(hdwf, AD_CHANNEL_1, true);
    FDwfAnalogInChannelEnableSet(hdwf, AD_CHANNEL_2, true);

    //set 5V pk2pk input range on channel 1
    FDwfAnalogInChannelRangeSet(hdwf, AD_CHANNEL_1, 5);
    //set 5V pk2pk input range on channel 2
    FDwfAnalogInChannelRangeSet(hdwf, AD_CHANNEL_2, 5);

    // set sample rateF
    FDwfAnalogInAcquisitionModeSet(hdwf, acqmodeSingle);
    FDwfAnalogInFrequencySet(hdwf, hzAcq);

    // get the maximum buffer size
    FDwfAnalogInBufferSizeInfo(hdwf, NULL, &cSamples);
    FDwfAnalogInBufferSizeSet(hdwf, cSamples);
    //printf("Buffer size: %d\n", cSamples);

    channel1Samples = new double[cSamples];
    channel2Samples = new double[cSamples];

    // configure trigger
    // disable auto trigger
    FDwfAnalogInTriggerAutoTimeoutSet(hdwf, 0);
    FDwfAnalogInTriggerSourceSet(hdwf, trigsrcDetectorAnalogIn);
    FDwfAnalogInTriggerTypeSet(hdwf, trigtypeEdge);
    FDwfAnalogInTriggerChannelSet(hdwf, AD_CHANNEL_2);
    FDwfAnalogInTriggerLevelSet(hdwf, 1.0);
    FDwfAnalogInTriggerPositionSet(hdwf, cSamples*0.1/hzAcq);
    FDwfAnalogInTriggerConditionSet(hdwf, trigcondRisingPositive);

    // wait at least 2 seconds with Analog Discovery for the offset to stabilize, before the first reading after device open or offset/range change
    usleep(2000000);
    #endif

    #if defined(BENCHMARK_ODROID_GPIO)
    char filenameGpio[BUFSIZ];
    int gpioControl =  open ("/sys/class/gpio/export", O_RDWR);
    if (gpioControl > 0)
    write (gpioControl, (char *)(std::to_string(TRIGGER_PIN)+"\n").c_str(), 3);
    sprintf(filenameGpio, "/sys/class/gpio/gpio%d/direction", TRIGGER_PIN);
    int gpioDirection = open(filenameGpio, O_RDWR);
    sprintf(filenameGpio, "/sys/class/gpio/gpio%d/value", TRIGGER_PIN);
    int gpioValue = open(filenameGpio, O_RDWR);
    if (gpioDirection > 0)
    write(gpioDirection, "out\n", 4);
    else
    BENCHMARK_ERROR("Could not initialize GPIO library");
    #endif

    #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
    SystemCounterState before_sstate, after_sstate;
    pcm_handle = PCM::getInstance();
    if (pcm_handle->program() != PCM::Success) {
      std::cout << "PCM::getInstance() failed!" << std::endl;
      exit(EXIT_FAILURE);
    }
    #elif defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
    // Create temporary counter storage
    int64_t event_values[NUM_EVENTS];

    //Initialize the low-level interface
    if ( PAPI_library_init( PAPI_VER_CURRENT ) != PAPI_VER_CURRENT ) {
      BENCHMARK_ERROR("Could not initialize PAPI library");
    }

    // Find the RAPL component
    int numcmp = PAPI_num_components();
    int rapl_cid = -1;
    const PAPI_component_info_t *cmpinfo = NULL;

    for(int cid = 0; cid < numcmp; cid++) {
      if ( (cmpinfo = PAPI_get_component_info(cid)) == NULL ) {
         BENCHMARK_ERROR("Could not enumerate PAPI component " + std::to_string(cid));
      }

      if ( strstr( cmpinfo->name, "rapl" ) ) {
        rapl_cid = cid;
        break;
      }
    }

    if (rapl_cid == -1) {
      BENCHMARK_ERROR("Could not discover any RAPL PAPI component");
    }

    // Set up our event set
    int rapl_eventset = PAPI_NULL;
    if ( PAPI_create_eventset( &rapl_eventset ) != PAPI_OK ) {
      BENCHMARK_ERROR("Could not create a PAPI event set");
    }

    // Put the events we want into the event set
    int code;

    if (PAPI_event_name_to_code( "rapl:::DRAM_ENERGY:PACKAGE0", &code ) != PAPI_OK) {
      BENCHMARK_ERROR("PAPI event code lookup failed for event rapl:::DRAM_ENERGY:PACKAGE0");
    } else {
      if ( PAPI_add_event( rapl_eventset, code ) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not add event to eventset: rapl:::DRAM_ENERGY:PACKAGE0 (code " + std::to_string(code) + ") (maybe too many events)");
      }
    }

    if (PAPI_event_name_to_code( "rapl:::DRAM_ENERGY_CNT:PACKAGE0", &code ) != PAPI_OK) {
      BENCHMARK_ERROR("PAPI event code lookup failed for event rapl:::DRAM_ENERGY_CNT:PACKAGE0");
    } else {
      if ( PAPI_add_event( rapl_eventset, code ) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not add event to eventset: rapl:::DRAM_ENERGY_CNT:PACKAGE0 (code " + std::to_string(code) + ") (maybe too many events)");
      }
    }

    if (PAPI_event_name_to_code( "rapl:::PACKAGE_ENERGY:PACKAGE0", &code ) != PAPI_OK) {
      BENCHMARK_ERROR("PAPI event code lookup failed for event rapl:::PACKAGE_ENERGY:PACKAGE0");
    } else {
      if ( PAPI_add_event( rapl_eventset, code ) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not add event to eventset: rapl:::PACKAGE_ENERGY:PACKAGE0 (code " + std::to_string(code) + ") (maybe too many events)");
      }
    }

    if (PAPI_event_name_to_code( "rapl:::PACKAGE_ENERGY_CNT:PACKAGE0", &code ) != PAPI_OK) {
      BENCHMARK_ERROR("PAPI event code lookup failed for event rapl:::PACKAGE_ENERGY_CNT:PACKAGE0");
    } else {
      if ( PAPI_add_event( rapl_eventset, code ) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not add event to eventset: rapl:::PACKAGE_ENERGY_CNT:PACKAGE0 (code " + std::to_string(code) + ") (maybe too many events)");
      }
    }
    #elif defined(USE_RAPL) && defined(BENCHMARK_PLATFORM_X64)
    int type;
    int config[NUM_RAPL_DOMAINS];
    char units[NUM_RAPL_DOMAINS][BUFSIZ];
    char filename[BUFSIZ];
    int fd[NUM_RAPL_DOMAINS][MAX_PACKAGES];
    double scale[NUM_RAPL_DOMAINS];
    struct perf_event_attr rapl_attr;
    long long rapl_value;

    detect_packages();

    FILE *fff = fopen("/sys/bus/event_source/devices/power/type","r");

    if (fff == NULL) {
      BENCHMARK_ERROR("No perf_event rapl support found (requires Linux 3.14)");
    }

    fscanf(fff,"%d",&type);
    fclose(fff);

    for(int domain = 0; domain < NUM_RAPL_DOMAINS; domain++) {
      // Try reading the counter
      sprintf(filename, "/sys/bus/event_source/devices/power/events/%s", rapl_domain_names[domain]);
      fff = fopen(filename, "r");

      if (fff != NULL) {
        fscanf(fff, "event=%x", &config[domain]);
        fclose(fff);
      } else {
        continue;
      }

      sprintf(filename, "/sys/bus/event_source/devices/power/events/%s.scale", rapl_domain_names[domain]);
      fff = fopen(filename, "r");

      if (fff != NULL) {
        fscanf(fff, "%lf", &scale[domain]);
        fclose(fff);
      } else {
        BENCHMARK_ERROR("Could not read RAPL domain scale: " + string(filename));
      }

      sprintf(filename, "/sys/bus/event_source/devices/power/events/%s.unit", rapl_domain_names[domain]);
      fff = fopen(filename, "r");

      if (fff != NULL) {
        fscanf(fff, "%s", units[domain]);
        fclose(fff);
      } else {
        BENCHMARK_ERROR("Could not read RAPL domain units: " + string(filename));
      }
    }
    #endif

    struct timespec host_ts_start;
    struct timespec host_ts_end;

    #if defined(BENCHMARK_PLATFORM_X64)
    uint64_t start, stop;
    #elif defined(BENCHMARK_PLATFORM_ARM) && defined(BENCHMARK_ARM_USE_PERF)
    struct perf_event_attr pe;
    uint64_t count;
    int fd;
    memset(&pe, 0, sizeof(struct perf_event_attr));
    pe.type = PERF_TYPE_HARDWARE;
    pe.size = sizeof(struct perf_event_attr);
    pe.config = PERF_COUNT_HW_CPU_CYCLES;
    pe.disabled = 1;
    pe.exclude_kernel = 1;
    pe.exclude_hv = 1;
    #endif

    data = new Measurements(cfg->runs);

    this->setup(cfg);

    for (uint64_t i = 1; i < cfg->runs+1; i++) {

      #if defined (ANALOG_DISCOVERY_2_POWER_MEASUREMENTS)
      // start Data acquisition with AD2
      FDwfAnalogInConfigure(hdwf, false, true);
      #endif

      //Insert sleep
      #if defined(SLEEP_BETWEEN_BENCHMARKS)
      usleep(SLEEP_BETWEEN_BENCHMARKS);
      #endif

      #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
      before_sstate = getSystemCounterState();
      #elif defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
      if ( PAPI_start(rapl_eventset) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not start PAPI eventset");
      }
      #elif defined(USE_RAPL) && defined(BENCHMARK_PLATFORM_X64)
      for(int package = 0; package < total_packages; package++) {
        for(int domain = 0; domain < NUM_RAPL_DOMAINS; domain++) {
          fd[domain][package] = -1;
          memset(&rapl_attr, 0x0, sizeof(rapl_attr));
          rapl_attr.type = type;
          rapl_attr.config = config[domain];

          if (config[domain] == 0) {
            continue;
          }

          //fd[domain][package] = perf_event_open(&rapl_attr, -1, package_map[package], -1, 0);
          fd[domain][package] = perf_event_open(&rapl_attr, -1, 0, -1, 0);

          if (fd[domain][package] < 0) {
            if (errno == EACCES) {
              BENCHMARK_ERROR("Access denied while reading RAPL values");
            }
            else {
              //BENCHMARK_ERROR("Error opening core " + std::to_string(package_map[package]) +
                                       //" config " + std::to_string(config[domain]) +
                                       //": " + string(strerror(errno)));
            }
          }
        }
      }
      #endif

      #if defined(BENCHMARK_ODROID_GPIO)
      if (gpioValue > 0)
      write(gpioValue, "1\n", 2);
      #endif

      clock_gettime(CLOCK_MONOTONIC, &host_ts_start);

      #if defined(BENCHMARK_ODROID_GPIO)
      if (gpioValue > 0)
      write(gpioValue, "1\n", 2);
      #endif

      #if defined(BENCHMARK_PLATFORM_X64)
      start = clocks();
      #elif defined(BENCHMARK_PLATFORM_ARM) && defined(BENCHMARK_ARM_USE_PERF)
      fd = perf_event_open(&pe, 0, -1, -1, 0);
      if (fd == -1) {
        fprintf(stderr, "Error opening leader %llx\n", pe.config);
        exit(EXIT_FAILURE);
      }

      ioctl(fd, PERF_EVENT_IOC_RESET, 0);
      ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
      #endif

      this->benchmark(cfg);

      #if defined(BENCHMARK_PLATFORM_X64)
      stop = clocks();
      #elif defined(BENCHMARK_PLATFORM_ARM) && defined(BENCHMARK_ARM_USE_PERF)
      ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
      read(fd, &count, sizeof(uint64_t));
      close(fd);
      #endif

      #if defined(BENCHMARK_ODROID_GPIO)
      if (gpioValue > 0)
      write(gpioValue, "0\n", 2);
      #endif

      clock_gettime(CLOCK_MONOTONIC, &host_ts_end);

      #if defined(BENCHMARK_ODROID_GPIO)
      if (gpioValue > 0)
      write(gpioValue, "0\n", 2);
      #endif

      #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
      after_sstate = getSystemCounterState();
      #elif defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
      if ( PAPI_stop( rapl_eventset, event_values ) != PAPI_OK ) {
        BENCHMARK_ERROR("Could not stop PAPI eventset");
      }
      #endif

      // save the data
      data->hostTimes[i-1]           = (((double) (host_ts_end.tv_sec - host_ts_start.tv_sec)) * 1e9)
                                       +
                                       ((double) (host_ts_end.tv_nsec - host_ts_start.tv_nsec));

      #if defined(BENCHMARK_PLATFORM_X64)
      data->deviceTimes[i-1]         = 0;
      data->hostCycles[i-1]          = stop - start;
      #elif defined(BENCHMARK_PLATFORM_ARM)
      data->deviceTimes[i-1]         = 0;
      #if defined(BENCHMARK_ARM_USE_PERF)
      data->hostCycles[i-1]          = count;
      #endif
      #endif

      #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
      data->instsPerClock[i-1]  = getIPC(before_sstate, after_sstate);
      data->l2Misses[i-1]       = getL2CacheMisses(before_sstate, after_sstate);

      double l2MissBase         = getCyclesLostDueL2CacheMisses(before_sstate, after_sstate);
      data->l2StallCycles[i-1]  = (uint64_t) ceil( (((double) (stop - start))/(l2MissBase+1.0)) * l2MissBase );

      data->l3Misses[i-1]       = getL3CacheMisses(before_sstate, after_sstate);

      double l3MissBase         = getCyclesLostDueL3CacheMisses(before_sstate, after_sstate);
      data->l3StallCycles[i-1]  = (uint64_t) ceil( (((double) (stop - start))/(l3MissBase+1.0)) * l3MissBase );

      data->bytesRead[i-1]      = getBytesReadFromMC(before_sstate, after_sstate);

      data->instsRetired[i-1]   = getInstructionsRetired(before_sstate, after_sstate);
      #elif defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
      data->dramEnergy[i-1]     = (double) event_values[0];
      data->dramCounts[i-1]     = event_values[1];
      data->packageEnergy[i-1]  = (double) event_values[2];
      data->packageCounts[i-1]  = event_values[3];

      #elif defined(USE_RAPL) && defined(BENCHMARK_PLATFORM_X64)
      for(int package = 0; package < total_packages; package++) {
        for(int domain = 0; domain < NUM_RAPL_DOMAINS; domain++) {
          if (fd[domain][package] != -1) {
            read(fd[domain][package], &rapl_value, 8);
            close(fd[domain][package]);

            switch (domain) {
              case 0: {
                data->coreCounts[i-1] = (int64_t) rapl_value;
                data->coreEnergy[i-1] = (((double) rapl_value)*(scale[domain] * 1e9));
              } break;
              case 1: {
                data->gpuCounts[i-1] = (int64_t) rapl_value;
                data->gpuEnergy[i-1] = (((double) rapl_value)*(scale[domain] * 1e9));
              } break;
              case 2: {
                data->packageCounts[i-1] = (int64_t) rapl_value;
                data->packageEnergy[i-1] = (((double) rapl_value)*(scale[domain] * 1e9));
              } break;
              case 3: {
                data->dramCounts[i-1] = (int64_t) rapl_value;
                data->dramEnergy[i-1] = (((double) rapl_value)*(scale[domain] * 1e9));
              } break;
              default: break;
            }
          }
        }
      }
      #endif

      #if defined (ANALOG_DISCOVERY_2_POWER_MEASUREMENTS)

      std::ofstream datastream;

      // re adjust sampling frequency after 1st run
      if (i==1) {
        hzAcq = (double) cSamples/(data->hostTimes[0]/1e9);
        FDwfAnalogInFrequencySet(hdwf, hzAcq*0.80); //Extra time for trigger
        FDwfAnalogInFrequencyGet(hdwf, &hzAcq);
        FDwfAnalogInTriggerPositionSet(hdwf, cSamples*0.45/hzAcq); //5% trigger
        //printf("%f/n", hzAcq);
        cfg->makeResultsFile(datastream, this->name().c_str(), "SamplingFreq");
        datastream << hzAcq << std::endl;
        datastream.flush();
        datastream.close();
        usleep(2000000);
      }
      else {
        do{
         if(!FDwfAnalogInStatus(hdwf, true, &sts)) {
           printf("error");
         }
        }while(sts != stsDone);

         // get the samples for each channel
         FDwfAnalogInStatusData(hdwf, AD_CHANNEL_1, channel1Samples, cSamples);
         FDwfAnalogInStatusData(hdwf, AD_CHANNEL_2, channel2Samples, cSamples);

         // write samples to file
         cfg->makeResultsFile(datastream, this->name().c_str(), ("AD2_signals"+std::to_string(i)).c_str());

         for(int k = 0; k < (int)cSamples; k++) {
           datastream << channel1Samples[k] << " " << channel2Samples[k] << std::endl;
         }

         datastream.flush();
         datastream.close();
      }
      #endif
    }

    #if defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
    if ( PAPI_cleanup_eventset( rapl_eventset ) != PAPI_OK ) {
      BENCHMARK_ERROR("Could not clean up PAPI eventset");
    }

    if ( PAPI_destroy_eventset( &rapl_eventset ) != PAPI_OK ) {
      BENCHMARK_ERROR("Could not destroy PAPI eventset");
    }
    #endif

    #if defined(BENCHMARK_ODROID_GPIO)
    if (gpioDirection > 0)
    close(gpioDirection);
    if (gpioValue > 0)
    close(gpioValue);
    if (gpioControl> 0)
    close(gpioControl);
    #endif

    #if defined (ANALOG_DISCOVERY_2_POWER_MEASUREMENTS)
    delete [] channel1Samples;
    delete [] channel2Samples;
    // close the device
    FDwfDeviceClose(hdwf);
    #endif

    this->teardown(cfg);
  }

  void cleanup() {
    #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
    pcm_handle->cleanup();
    #endif

    delete data;
  }

};

class BenchmarkRunner {
  BenchmarkConfig *cfg;
  std::vector<Benchmark*> bmarks;
public:

  BenchmarkRunner(BenchmarkConfig *c, std::vector<Benchmark*> b)
  : cfg(c), bmarks(b) {}

  template <typename T> void write(uint64_t runs, T *data, const char *id, const char *statName) {
    std::ofstream datastream;

    cfg->makeResultsFile(datastream, id, statName);

    for(int k = 0; k < (int)runs; k++) {
      datastream << std::fixed << std::setprecision(12) << std::to_string(data[k]) << std::endl;
    }

    datastream.flush();
    datastream.close();
  }

  void run() {
    for (typename std::vector< Benchmark*>::iterator i = bmarks.begin(); i != bmarks.end(); i++) {
      (*i)->run(cfg);

      #if defined(BENCHMARK_PLATFORM_X64) || (defined(BENCHMARK_PLATFORM_ARM) && defined(BENCHMARK_ARM_USE_PERF))
      write(cfg->runs, (*i)->data->hostCycles,    (*i)->name().c_str(), "hcy");
      #endif

      write(cfg->runs, (*i)->data->hostTimes,     (*i)->name().c_str(), "hns");

      #if defined(USE_PCM) && defined(BENCHMARK_PLATFORM_X64)
      write(cfg->runs, (*i)->data->instsPerClock, (*i)->name().c_str(), "ipc");
      write(cfg->runs, (*i)->data->l2Misses,      (*i)->name().c_str(), "l2m");
      write(cfg->runs, (*i)->data->l2StallCycles, (*i)->name().c_str(), "l2s");
      write(cfg->runs, (*i)->data->l3Misses,      (*i)->name().c_str(), "l3m");
      write(cfg->runs, (*i)->data->l3StallCycles, (*i)->name().c_str(), "l3s");
      write(cfg->runs, (*i)->data->bytesRead,     (*i)->name().c_str(), "xfr");
      write(cfg->runs, (*i)->data->instsRetired,  (*i)->name().c_str(), "ret");
      #elif defined(USE_PAPI) && defined(BENCHMARK_PLATFORM_X64)
      write(cfg->runs, (*i)->data->dramEnergy,    (*i)->name().c_str(), "dram-energy");
      write(cfg->runs, (*i)->data->dramCounts,    (*i)->name().c_str(), "dram-counts");
      write(cfg->runs, (*i)->data->packageEnergy, (*i)->name().c_str(), "package-energy");
      write(cfg->runs, (*i)->data->packageCounts, (*i)->name().c_str(), "package-counts");
      #elif defined(USE_RAPL) && defined(BENCHMARK_PLATFORM_X64)
      write(cfg->runs, (*i)->data->dramEnergy,    (*i)->name().c_str(), "dram-energy");
      write(cfg->runs, (*i)->data->dramCounts,    (*i)->name().c_str(), "dram-counts");
      write(cfg->runs, (*i)->data->packageEnergy, (*i)->name().c_str(), "package-energy");
      write(cfg->runs, (*i)->data->packageCounts, (*i)->name().c_str(), "package-counts");
      write(cfg->runs, (*i)->data->coreEnergy,    (*i)->name().c_str(), "core-energy");
      write(cfg->runs, (*i)->data->coreCounts,    (*i)->name().c_str(), "core-counts");
      write(cfg->runs, (*i)->data->gpuEnergy,     (*i)->name().c_str(), "gpu-energy");
      write(cfg->runs, (*i)->data->gpuCounts,     (*i)->name().c_str(), "gpu-counts");
      #endif
      (*i)->cleanup();
    }
  }

};

}

#endif // defined(BENCHMARK_UTILS_H)
