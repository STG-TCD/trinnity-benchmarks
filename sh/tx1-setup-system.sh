#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

# Kill the ondemand governor, just in case
sudo update-rc.d -f ondemand remove

# Maximize CPU clocks
sudo echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable

sudo echo 1 > /sys/devices/system/cpu/cpu0/online
sudo echo 1 > /sys/devices/system/cpu/cpu1/online
sudo echo 1 > /sys/devices/system/cpu/cpu2/online
sudo echo 1 > /sys/devices/system/cpu/cpu3/online

sudo echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
sudo echo performance > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
sudo echo performance > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
sudo echo performance > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor

# Maximize GPU core clocks
#cat /sys/kernel/debug/clock/gpu_dvfs_t
#cat /sys/kernel/debug/clock/dvfs_table

sudo cat /sys/kernel/debug/clock/gbus/max > /sys/kernel/debug/clock/override.gbus/rate
sudo echo 1 > /sys/kernel/debug/clock/override.gbus/state

# Maximize GPU memory clocks
sudo cat /sys/kernel/debug/clock/emc/max > /sys/kernel/debug/clock/override.emc/rate
sudo echo 1 > /sys/kernel/debug/clock/override.emc/state

# Turn the fan all the way up
sudo echo 255 > /sys/kernel/debug/tegra_fan/target_pwm
