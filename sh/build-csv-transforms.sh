#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

# This script needs to be run from the top level project directory for this to work
source sh/definitions.sh
source ${SCENARIO_DEFINITIONS}

: ${STATS:=build/stats}
: ${GRAPH_METRICS:=hns}
: ${SCENARIOS:="1 2 3 4"}

mkdir -p $STATS/

for stat in $GRAPH_METRICS
do
  csvFile=$STATS/$stat-transforms.csv
  rm -f $csvFile

  #awk uses 1-based indexing
  for parameter in 2 4 5
  do
    tmpFile=`mktemp`

    for scenario in $SCENARIOS
    do
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk "{printf \"%u, \", \$$parameter}" >> $tmpFile
    done

    echo -n "${PARAMETER_NAMES[$parameter]}, " >> $csvFile
    cat $tmpFile | sed 's/, $//' >> $csvFile
    echo >> $csvFile
    rm -f $tmpFile
  done

  for method in ${TRANSFORM_DISPLAY_NAME[@]}
  do
    statDatfile=$STATS/summary-$stat-$method.dat

    [ -e $statDatfile ] &&
    echo "Building $stat cost table for $method" &&
    echo -n "${TRANSFORM_DISPLAY_NAME[$method]}, " >> $csvFile
    cat $statDatfile | gawk '{printf "%f, ", $2}' | sed 's/, $//' >> $csvFile &&
    echo >> $csvFile
  done
done

exit 0
