#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

# This script needs to be run from the top level project directory
source sh/definitions.sh

: ${TITLE:=MCMK Shootout}
: ${STATS:=build/stats}
: ${GRAPHS:=build/graphs}
: ${GRAPH_METHODS:=ground}
: ${GRAPH_METRICS:=hns}
: ${SCENARIOS:="1 2 3 4"}
: ${PREFIX:="summary"}
: ${XTIC_ROTATE:=0}
: ${PLOT_WIDTH:=0.4}
: ${PLOT_HEIGHT:=1.05}
: ${PLOT_KEY:="outside bottom center horizontal nobox font \",12\" samplen 0.3"}
: ${IM_EXT:="pdf"}

(>&2 echo "Plotting scenarios $SCENARIOS")

plot_pro_prologue="set terminal ${IM_EXT} font \",13\""

read -d '' plot_prologue <<- EOF

set style fill solid 1.0 border -1
set grid y
set colorsequence podo
set tmargin 2
set xtics mirror offset 1,0 scale 0 font ",13"
set ytics mirror

set ytics scale 0

f(x) = 1.0

EOF

for stat in $GRAPH_METRICS
do
  plotfile="$GRAPHS/$stat.plt"
  imagefile="$GRAPHS/$stat."$IM_EXT
  echo "reset" > $plotfile
  echo "$plot_pro_prologue" >> $plotfile
  echo "$plot_prologue" >> $plotfile

  S=(${SCENARIOS[@]})
  NUM_SCENARIOS=(${#S[@]})
  M=(${GRAPH_METHODS[@]})
  NUM_METHODS=(${#M[@]})

  xtic_rotate="$XTIC_ROTATE"
  echo "set xtics rotate by $xtic_rotate" >> $plotfile

  REAL_WIDTH=$(echo \"$NUM_SCENARIOS*$PLOT_WIDTH\" | bc)

  #echo "set size $REAL_WIDTH,$PLOT_HEIGHT" >> $plotfile

  echo "set out \"$imagefile\"" >> $plotfile
  echo "set origin 0,0" >> $plotfile
  echo "set boxwidth 0.10" >> $plotfile
  if [ -v YLABEL ]
  then
    echo "set ylabel \"$YLABEL\"" >> $plotfile
  else
    echo "set ylabel \"${STAT_DISPLAY_NAME[$stat]}\"" >> $plotfile
  fi

  echo "set title \"$TITLE\"" >> $plotfile
  echo "set key $PLOT_KEY" >> $plotfile
  echo "set yrange [0:]" >> $plotfile
  echo "set xrange [-0.10:$(echo \"$NUM_SCENARIOS - \(1 - 0.10*$NUM_METHODS\)\" | bc)]" >> $plotfile
  #echo "set logscale y 10" >> $plotfile

  echo >> $plotfile

  METHOD=(${GRAPH_METHODS[@]})

  echo -n "plot" >> $plotfile
  echo "     \"$STATS/$PREFIX-$stat-${METHOD[0]}.dat\"   using (column(0)):2:3              with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[0]}]}\",\\" >> $plotfile
  echo "     \"$STATS/$PREFIX-$stat-${METHOD[1]}.dat\"   using (column(0)+0.10):2:3:xtic(1) with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[1]}]}\",\\" >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[2]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[2]}.dat\"   using (column(0)+0.20):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[2]}]}\",\\" >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[3]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[3]}.dat\"   using (column(0)+0.30):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[3]}]}\",\\"  >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[4]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[4]}.dat\"   using (column(0)+0.40):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[4]}]}\",\\"  >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[5]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[5]}.dat\"   using (column(0)+0.50):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[5]}]}\",\\"  >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[6]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[6]}.dat\"   using (column(0)+0.60):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[6]}]}\",\\"  >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[7]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[7]}.dat\"   using (column(0)+0.70):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[7]}]}\",\\"  >> $plotfile
  [ -e $STATS/$PREFIX-$stat-${METHOD[8]}.dat ] && echo "     \"$STATS/$PREFIX-$stat-${METHOD[8]}.dat\"   using (column(0)+0.80):2:3         with boxerrorbars title \"${METHOD_DISPLAY_NAME[${METHOD[8]}]}\",\\"  >> $plotfile

  gnuplot -p $plotfile #1>/dev/null 2>/dev/null
done

exit 0
