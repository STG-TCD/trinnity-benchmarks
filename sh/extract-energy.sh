#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

# This script needs to be run from the top level project directory for this to work
source sh/definitions.sh

: ${RESULTS:=build/results}
: ${STATS:=build/stats}
: ${GRAPH_METHODS:=ground}
: ${GRAPH_METRICS:=hns}
: ${RUNS:=10}
: ${SCENARIOS:="1 2 3 4"}
: ${DISCARD:=1}
: ${RSHUNT:=0.05}

mkdir -p $STATS/

echo "Processing"

for method in ${GRAPH_METHODS[@]}
do
    for scenario in $SCENARIOS
    do
	energyDatfile=$RESULTS/scenario/$scenario/$method/board-energy.dat
	rm -f $energyDatfile
	for x in `seq 1 $DISCARD`
	do
	    echo "0.0" >> $energyDatfile
	done

	SAMPLINGFREQ=$(awk "NR==1" $RESULTS/scenario/$scenario/$method/SamplingFreq.dat)

	for run in `seq $((DISCARD+1)) $RUNS`
	do
	    cat $RESULTS/scenario/$scenario/$method/AD2_signals$run.dat | awk -v Fs=$SAMPLINGFREQ -v Rshunt=$RSHUNT -f awk/extract-window-power-measurements.awk >> $energyDatfile
	done
    done
done


exit 0
