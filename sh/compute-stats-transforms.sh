#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

# This script needs to be run from the top level project directory for this to work
source sh/definitions.sh
source ${SCENARIO_DEFINITIONS}

: ${RESULTS:=build/results}
: ${STATS:=build/stats}
: ${GRAPH_METRICS:=hns}
: ${SCENARIOS:="1 2 3 4"}
: ${DISCARD:=1}

DISCARD=$((DISCARD+1))

mkdir -p $STATS/

for stat in $GRAPH_METRICS
do
  for method in ${TRANSFORM_DISPLAY_NAME[@]}
  do
    statDatfile=$STATS/summary-$stat-$method.dat
    rm -f $statDatfile

    for scenario in $SCENARIOS
    do
      [ -e $RESULTS/transforms/scenario/$scenario/$method/$stat.dat ] &&
      echo -n "$scenario " >> $statDatfile &&
      echo "Processing $stat for $method in scenario $scenario" &&
      sort --numeric-sort --reverse $RESULTS/transforms/scenario/$scenario/$method/$stat.dat \
      | tail -n +$DISCARD \
      | python -c "import sys;import pandas;df=pandas.read_csv(sys.stdin, sep=\" \", header=None, usecols=[0]);print(\"{} {} {}\".format(df.mean()[0], df.std()[0], (df.std()[0]/(df.mean()[0]/100.0))), end='')" \
      >> $statDatfile &&
      echo >> $statDatfile

      [ ! -e $RESULTS/transforms/scenario/$scenario/$method/$stat.dat ] &&
      echo -n "$scenario 0 0 0" >> $statDatfile &&
      echo >> $statDatfile
    done
  done
done

exit 0
