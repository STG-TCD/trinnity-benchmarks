#!/bin/bash
#
#Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
#of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
#near Dublin
#

: ${HEADER:=${BUILDDIR}/scenarios.h}

echo "Loading microbenchmark scenario definitions from ${SCENARIO_DEFINITIONS}"

source ${SCENARIO_DEFINITIONS}

mkdir -p build/
rm -f $HEADER

start=0

for scenario in $SCENARIOS
do
  if [[ "${SCENARIO_PARAMETERS[$scenario]}" ]]
  then
    K_W=`echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "%u", $6}'`
    K_H=`echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "%u", $7}'`
    if [[ "$K_H" != "0" && "$K_W" != "0" ]]
    then
      echo "$scenario ${SCENARIO_PARAMETERS[$scenario]}" | awk '{printf "Processing microbenchmark scenario %u: M=%u C=%u S=%u W=%u H=%u K_W=%u K_H=%u\n", $1, $2, $3, $4, $5, $6, $7, $8}'
      echo "#if (SCENARIO == $start)" >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define KERNELS               %u\n", $1}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define CHANNELS              %u\n", $2}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define STRIDE                %u\n", $3}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define IMG_W                 %u\n", $4}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define IMG_H                 %u\n", $5}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define K_W                   %u\n", $6}' >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define K_H                   %u\n", $7}' >> $HEADER
      echo                                                   "#if !defined(KERNEL_SPARSITY_RATIO)"      >> $HEADER
      echo ${SCENARIO_PARAMETERS[$scenario]} | gawk '{printf "#define KERNEL_SPARSITY_RATIO %u\n", $8}' >> $HEADER
      echo                                                   "#endif"                                   >> $HEADER
      echo "#endif // (SCENARIO == $start)" >> $HEADER
      echo >> $HEADER
      (( start++ ))
    fi
  fi
done

exit 0
