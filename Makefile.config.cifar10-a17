# What scenarios to build
CROSS_SCENARIO_DEFINITIONS=../triNNity-demos/models/deployment/cifar10-scenarios
SCENARIO_DEFINITIONS=../triNNity-demos/models/deployment/cifar10-scenarios
SCENARIOS = $(shell [[ -f $(SCENARIO_DEFINITIONS) ]] && seq 0 `wc -l $(SCENARIO_DEFINITIONS) | awk '{print $$1 - 5}'`)
CROSS_SCENARIOS = $(shell [[ -f $(CROSS_SCENARIO_DEFINITIONS) ]] && seq 0 `wc -l $(CROSS_SCENARIO_DEFINITIONS) | awk '{print $$1 - 5}'`)

CROSS_TSCENARIO_DEFINITIONS=../triNNity-demos/models/deployment/cifar10-transforms
TSCENARIO_DEFINITIONS=../triNNity-demos/models/deployment/cifar10-transforms
TSCENARIOS = $(shell [[ -f $(TSCENARIO_DEFINITIONS) ]] && seq 0 `wc -l $(TSCENARIO_DEFINITIONS) | awk '{print $$1 - 5}'`)
CROSS_TSCENARIOS = $(shell [[ -f $(CROSS_TSCENARIO_DEFINITIONS) ]] && seq 0 `wc -l $(CROSS_TSCENARIO_DEFINITIONS) | awk '{print $$1 - 5}'`)

# Where to run the benchmarks
CROSS_BENCHMARK_HOST=linaro@stg-a17-devkit
CROSS_BENCHMARK_PATH=triNNity-benchmarks-a17

# How many benchmark iterations to do
RUNS = 150

# How many threads to use for multithreaded convolutions
THREADS = 4

# What cores to use for multithreaded benchmarks
MULTI_CORES = 0,1,2,3

# How many iterations to discard for results
DISCARD = 100

# What methods to benchmark
METHODS = \
-DBENCHMARK_TRINNITY_SUMS \
-DBENCHMARK_TRINNITY_IM2ROW_SCAN_A_B_I_K \
-DBENCHMARK_TRINNITY_IM2ROW_SCAN_AT_BT_K_I \
-DBENCHMARK_TRINNITY_IM2ROW_SCAN_A_BT_I_K \
-DBENCHMARK_TRINNITY_IM2ROW_SCAN_A_BT_K_I \
-DBENCHMARK_TRINNITY_IM2ROW_COPY_SHORT_A_B_I_K \
-DBENCHMARK_TRINNITY_IM2ROW_COPY_SHORT_AT_BT_K_I \
-DBENCHMARK_TRINNITY_IM2ROW_COPY_SHORT_A_BT_I_K \
-DBENCHMARK_TRINNITY_IM2ROW_COPY_SHORT_A_BT_K_I \
-DBENCHMARK_TRINNITY_IM2COL_SCAN_AT_B_I_K \
-DBENCHMARK_TRINNITY_IM2COL_SCAN_AT_B_K_I \
-DBENCHMARK_TRINNITY_IM2COL_SCAN_AT_BT_I_K \
-DBENCHMARK_TRINNITY_IM2COL_SCAN_A_B_K_I \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SHORT_AT_B_I_K \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SHORT_AT_B_K_I \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SHORT_AT_BT_I_K \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SHORT_A_B_K_I \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SELF_AT_B_I_K \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SELF_AT_B_K_I \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SELF_AT_BT_I_K \
-DBENCHMARK_TRINNITY_IM2COL_COPY_SELF_A_B_K_I \
-DBENCHMARK_TRINNITY_K2R \
-DBENCHMARK_TRINNITY_K2C \
-DBENCHMARK_TRINNITY_K2RAS \
-DBENCHMARK_TRINNITY_K2CAS \
-DBENCHMARK_TRINNITY_K2RAA_AB \
-DBENCHMARK_TRINNITY_K2RAA_ABT \
-DBENCHMARK_TRINNITY_K2RAA_ATB \
-DBENCHMARK_TRINNITY_K2RAA_ATBT \
-DBENCHMARK_TRINNITY_WINOGRAD_2_3 \
-DBENCHMARK_TRINNITY_WINOGRAD_2x2_3x3 \
-DBENCHMARK_TRINNITY_WINOGRAD_3_3 \
-DBENCHMARK_TRINNITY_WINOGRAD_3x3_3x3 \
-DBENCHMARK_TRINNITY_WINOGRAD_4x4_3x3 \
-DBENCHMARK_TRINNITY_WINOGRAD_2_5 \
-DBENCHMARK_TRINNITY_WINOGRAD_2x2_5x5 \
-DBENCHMARK_TRINNITY_WINOGRAD_3_5 \
-DBENCHMARK_TRINNITY_WINOGRAD_3x3_5x5 \
-DBENCHMARK_TRINNITY_WINOGRAD_4x4_5x5 \
-DBENCHMARK_TRINNITY_WINOGRAD_2_3_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_2x2_3x3_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_3_3_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_3x3_3x3_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_4x4_3x3_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_2_5_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_2x2_5x5_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_3_5_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_3x3_5x5_VEC_4 \
-DBENCHMARK_TRINNITY_WINOGRAD_4x4_5x5_VEC_4 \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_A_B_I_K \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_A_BT_I_K \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_AT_B_I_K \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_AT_BT_I_K \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_A_B_K_I \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_A_BT_K_I \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_AT_B_K_I \
-DBENCHMARK_TRINNITY_CONV_1x1_GEMM_AT_BT_K_I \

# What compiler to use to build
CXX = arm-linux-gnueabihf-g++

# Our specific build options
CONFIG_FLAGS = -DBENCHMARK_ERROR_FULL -DBENCHMARK_CBLAS -DBENCHMARK_DENSE -DBENCHMARK_SPECTRAL \
               -DTRINNITY_USE_GCC_VECTOR_EXTS -DTRINNITY_USE_OPENMP \
               -DTRINNITY_DISABLE_LAYER_TIMING -DTRINNITY_NO_EXCEPTIONS \
               -DWINOGRAD_MEASURE_KERNEL_TRANSFORM=false

# What compiler flags to use to build
CXXFLAGS = -static -fno-rtti -mcpu=cortex-a17 $(AARCH32_CROSS_GCC_FLAGS) $(ARMCL_AARCH32_CROSS_FLAGS) $(OPT_LEVEL_GCC_O3_ONLY)

# What compiler flags to use to build in debug mode
DEBUG_CXXFLAGS = -static -mcpu=cortex-a17 $(AARCH32_CROSS_GCC_FLAGS) $(ARMCL_AARCH32_CROSS_FLAGS) $(OPT_LEVEL_GCC_DEBUG)

# What linker flags to use
LDFLAGS = -pthread -l:libgomp.a -l:libopenblas.a

# What metrics to compute
METRICS = hns

# What methods to run
ENABLE_METHODS = direct-sum2d-chw \
                 direct-sum2d-hwc \
                 im2row-scan-ab-ik \
                 im2row-scan-atbt-ki \
                 im2row-scan-abt-ik \
                 im2row-scan-abt-ki \
                 im2row-copy-short-ab-ik \
                 im2row-copy-short-atbt-ki \
                 im2row-copy-short-abt-ik \
                 im2row-copy-short-abt-ki \
                 im2col-scan-atb-ik \
                 im2col-scan-atb-ki \
                 im2col-scan-atbt-ik \
                 im2col-scan-ab-ki \
                 im2col-copy-short-atb-ik \
                 im2col-copy-short-atb-ki \
                 im2col-copy-short-atbt-ik \
                 im2col-copy-short-ab-ki \
                 im2col-copy-self-atb-ik \
                 im2col-copy-self-atb-ki \
                 im2col-copy-self-atbt-ik \
                 im2col-copy-self-ab-ki \
                 kn2row \
                 kn2col \
                 kn2row-as \
                 kn2col-as \
                 kn2row-aa-ab \
                 kn2row-aa-abt \
                 kn2row-aa-atb \
                 kn2row-aa-atbt \
                 conv-1x1-gemm-ab-ik \
                 conv-1x1-gemm-abt-ik \
                 conv-1x1-gemm-atb-ik \
                 conv-1x1-gemm-atbt-ik \
                 conv-1x1-gemm-ab-ki \
                 conv-1x1-gemm-abt-ki \
                 conv-1x1-gemm-atb-ki \
                 conv-1x1-gemm-atbt-ki \
                 winograd-2-3 \
                 winograd-2x2-3x3 \
                 winograd-3-3 \
                 winograd-3x3-3x3 \
                 winograd-4x4-3x3 \
                 winograd-2-5 \
                 winograd-2x2-5x5 \
                 winograd-3-5 \
                 winograd-3x3-5x5 \
                 winograd-4x4-5x5 \
                 winograd-2-3-vec-4 \
                 winograd-2x2-3x3-vec-4 \
                 winograd-3-3-vec-4 \
                 winograd-3x3-3x3-vec-4 \
                 winograd-4x4-3x3-vec-4 \
                 winograd-2-5-vec-4 \
                 winograd-2x2-5x5-vec-4 \
                 winograd-3-5-vec-4 \
                 winograd-3x3-5x5-vec-4 \
                 winograd-4x4-5x5-vec-4 \

# What methods to plot
PLOT_METHODS = $(ENABLE_METHODS)
