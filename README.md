## Contributing to the benchmarks

The file CONTRIBUTING.md explains how the benchmarks are structured, and
contains instructions for contributing new benchmark code, or adding new
methods to the benchmarking process.

### Creating the build environment ###

We provide Dockerfiles (in the `triNNity-demos` project) for bringing up the
benchmarking environment. The instructions that follow in this section presume
that Docker is being used to run the benchmarks.

You can run the benchmarks outside of docker, but if you do so, it becomes your
responsibility to ensure that the build environment is set up correctly!

Install Docker and start the Docker service.

To create the build environment, at the top level in the `triNNity-demos` repository say

`docker build -t trinnity-benchmarks -f Dockerfile .`

You may need to give your user permission to use docker with

`gpasswd -a $USER docker`

To get a shell in the build environment, say

`docker run -i -t trinnity-benchmarks /bin/bash`

### Updating the triNNity DNN tools ###

Because Docker does not support disabling the cache for individual
build steps, the final build steps that build and install the triNNity
DNN toolkit must be run outside Docker (or else you will be stuck with
stale versions of the software in the Docker cache).

Inside the build environment, you can say `make update` to update the
triNNity library, compiler, and optimizer. To update triNNity-caffe say
either `make update-caffe-cpu` or `make update-caffe-gpu` depending on
which you are using.

## Benchmarking ##

To use a microbenchmarking configuration generated from the `triNNity-demos`
project, the variables `SCENARIO_DEFINITIONS` (for convolutional scenarios) and
`TSCENARIO_DEFINITIONS` (for data layout transformation scenarios) control
which microbenchmarking scenario files to use.

Some pre-written configurations are provided in this repository, for example,
`Makefile.config.cifar10-x86_64` or `Makefile.config.imagenet-x86_64`. If there
is no config for the scenario you want to run, simply copy-and-paste the
contents of one of these configurations into a new
Makefile.config.<YOUR_SCENARIO>, and set the `SCENARIO_DEFINITIONS` and
`TSCENARIO_DEFINITIONS` variables to point at the microbenchmarking config
files that you wish to use.

To run the microbenchmarking process on the host machine, say
`make end-to-end-{single,multi}threaded-<CONFIG>`. For example, to run the CIFAR10
microbenchmarks in singlethreaded mode on a host x86_64 machine, say
`make end-to-end-singlethreaded-cifar10-x86_64`.

To speed up the build, you can also use the `-j N` argument to make for an
N-way parallel build.

### Cross-compilation ###

We support cross-compilation and microbenchmarking on remote machines. Just add
the prefix `cross-` to your make target. For example,
`make cross-end-to-end-multithreaded-cifar10-a57` cross-compiles the microbenchmarks
for CIFAR10 for the Cortex-A57, and executes the benchmarks remotely, pulling
the results back to your local `stats` directory.

For cross compilation, two extra variables control the execution of remote
commands:

- `CROSS_BENCHMARK_HOST` should be of the form `user@remote-hostname`
- `CROSS_BENCHMARK_PATH` should be a relative path in the home directory of the user on the remote host (benchmark temporary files and executables are stored here)


### Manual microbenchmarking ###

To manually select a configuration to build, execute this command:

`ln -sf <YOUR_CONFIG> Makefile.config`

You can say `make info` to show what will be built and run using the current
config.

You can clean with `make clean`, and use `make build run stats plot` to get
results.

If you would like to run a parallel build, you can use the `-j` argument to
make, e.g. `make -j4 build`.

If you do so, you will have to break your commands up as
`make -j4 build && make run stats plot`, because Make targets other than `build` do not support
multithreaded execution.

To generate the cost model files for the triNNity-optimizer, use the `make csv`
target.
